import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);

import products from "./modules/products";
import activity from "./modules/activity";
import account from "./modules/account";
import transactions from "./modules/transactions";

import auth from "@/modules/auth/store";
import cart from "@/modules/cart/store";

export default new Vuex.Store({
  modules: {
    products,
    activity,
    auth,
    account,
    cart,
    transactions
  },
  state: {
    displayAuth: false
  },
  mutations: {
    displayAuth(state, payload) {
      state.displayAuth = payload;
    }
  },
  actions: {},
  getters: {
    displayAuth(state) {
      return state.displayAuth;
    }
  }
});
