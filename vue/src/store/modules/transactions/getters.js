const getAccepted = state => {
  return state.accepted;
};

const getPendings = state => {
  return state.pendings;
};

const getOnGoing = state => {
  return state.onGoing;
};

const getCompleted = state => {
  return state.completed;
};

const getFavorites = state => {
  return state.favorites;
};

export default {
  getPendings,
  getOnGoing,
  getCompleted,
  getFavorites,
  getAccepted
};
