import actions from "./actions";
import getters from "./getters";
import mutations from "./mutations";

const state = {
  accepted: {
    data: null,
    pagination: null
  },

  pendings: {
    data: null,
    pagination: null
  },

  onGoing: {
    data: null,
    pagination: null
  },

  completed: {
    data: null,
    pagination: null
  },

  favorites: {
    data: null,
    pagination: null
  }
};

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
};
