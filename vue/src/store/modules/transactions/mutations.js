const setAccepted = (state, payload) => {
  state.accepted.data = payload.data;
  state.accepted.pagination = payload.pagination;
};

const setPendings = (state, payload) => {
  state.pendings.data = payload.data;
  state.pendings.pagination = payload.pagination;
};

const setOnGoing = (state, payload) => {
  state.onGoing.data = payload.data;
  state.onGoing.pagination = payload.pagination;
};

const setCompleted = (state, payload) => {
  state.completed.data = payload.data;
  state.completed.pagination = payload.pagination;
};

const setFavorites = (state, payload) => {
  state.favorites.data = payload.data;
  state.favorites.pagination = payload.pagination;
};

export default {
  setAccepted,
  setPendings,
  setOnGoing,
  setCompleted,
  setFavorites
};
