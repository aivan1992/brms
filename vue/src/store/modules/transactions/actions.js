import Axios from "axios";

const getAccepted = ({ commit }, payload) => {
  return new Promise((resolve, reject) => {
    /**
     *  @params payload.data, payload.nextPrev
     *
     *  Set empty string nextPrev if undefined otherwise append the nextPrev to url
     *
     */

    let nextPrev = payload.nextPrev != undefined ? payload.nextPrev : "";
    /**/
    Axios.get(`transactions/accepted?${nextPrev}`, vm.getOath2Token())
      .then(res => {
        if (res.status == 200) {
          commit("setAccepted", res.data);
          resolve(true);
        }
      })
      .catch(err => {
        reject(err);
      });
  });
};

const getPendings = ({ commit }, payload) => {
  return new Promise((resolve, reject) => {
    /**
     *  @params payload.data, payload.nextPrev
     *
     *  Set empty string nextPrev if undefined otherwise append the nextPrev to url
     *
     */

    let nextPrev = payload.nextPrev != undefined ? payload.nextPrev : "";
    /**/
    Axios.get(`transactions/pendings?${nextPrev}`, vm.getOath2Token())
      .then(res => {
        if (res.status == 200) {
          commit("setPendings", res.data);
          resolve(true);
        }
      })
      .catch(err => {
        reject(err);
      });
  });
};

const getOnGoing = ({ commit }, payload) => {
  return new Promise((resolve, reject) => {
    /**
     *  @params payload.data, payload.nextPrev
     *
     *  Set empty string nextPrev if undefined otherwise append the nextPrev to url
     *
     */

    let nextPrev = payload.nextPrev != undefined ? payload.nextPrev : "";
    /**/
    Axios.get(`transactions/on-going?${nextPrev}`, vm.getOath2Token())
      .then(res => {
        if (res.status == 200) {
          commit("setOnGoing", res.data);
          resolve(true);
        }
      })
      .catch(err => {
        reject(err);
      });
  });
};

const getCompleted = ({ commit }, payload) => {
  return new Promise((resolve, reject) => {
    /**
     *  @params payload.data, payload.nextPrev
     *
     *  Set empty string nextPrev if undefined otherwise append the nextPrev to url
     *
     */

    let nextPrev = payload.nextPrev != undefined ? payload.nextPrev : "";
    /**/
    Axios.get(`transactions/completed?${nextPrev}`, vm.getOath2Token())
      .then(res => {
        if (res.status == 200) {
          commit("setCompleted", res.data);
          resolve(true);
        }
      })
      .catch(err => {
        reject(err);
      });
  });
};

const getFavorites = ({ commit }, payload) => {
  return new Promise((resolve, reject) => {
    /**
     *  @params payload.data, payload.nextPrev
     *
     *  Set empty string nextPrev if undefined otherwise append the nextPrev to url
     *
     */

    let nextPrev = payload.nextPrev != undefined ? payload.nextPrev : "";
    /**/
    Axios.get(`transactions/favorites?${nextPrev}`, vm.getOath2Token())
      .then(res => {
        if (res.status == 200) {
          commit("setFavorites", res.data);
          resolve(true);
        }
      })
      .catch(err => {
        reject(err);
      });
  });
};

const requestRentalCancellation = ({ commit, state }, payload) => {
  return new Promise((resolve, reject) => {
    Axios.post(
      "transactions/request-rental-cancellation",
      payload,
      vm.getOath2Token()
    )
      .then(res => {
        if (res.status == 200) {
          resolve(res.data);
        }
      })
      .catch(err => {
        reject(err);
      });
  });
};

export default {
  getAccepted,
  getPendings,
  getOnGoing,
  getCompleted,
  getFavorites,
  requestRentalCancellation
};
