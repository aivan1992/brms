const setNewArrivals = (state, payload) => {
  state.newArrivals = payload.data;
};

const setAllProducts = (state, payload) => {
  state.allProducts.data = payload.data;
  state.allProducts.pagination = payload.pagination;
};

const setCategories = (state, payload) => {
  state.categories = payload.data;
};

const setSingleProduct = (state, payload) => {
  state.singleProduct = payload;
};

const setSearchProduct = (state, payload) => {
  state.searchProduct = payload;
};

export default {
  setNewArrivals,
  setAllProducts,
  setCategories,
  setSingleProduct,
  setSearchProduct
};
