import Axios from "axios";

const getAllProducts = ({ commit }, payload) => {
  /**
   *  @params payload.data, payload.nextPrev
   *
   *  Set empty string nextPrev if undefined otherwise append the nextPrev to url
   *
   */

  let nextPrev = payload.nextPrev != undefined ? payload.nextPrev : "";
  /**/

  /**
   * Convert JSON to URL query string
   *
   */
  var queryString = Object.keys(payload.data)
    .map(key => {
      return (
        encodeURIComponent(key) + "=" + encodeURIComponent(payload.data[key])
      );
    })
    .join("&");
  /**/

  return new Promise((resolve, reject) => {
    Axios.get(`products?${queryString} ${nextPrev}`, vm.getOath2Token())
      .then(res => {
        if (res.status == 200) {
          console.log(res.data);
          commit("setAllProducts", res.data);
          resolve(true);
        }
      })
      .catch(err => {
        reject(err);
      });
  });
};

const getNewArrivals = ({ commit }, payload) => {
  return new Promise((resolve, reject) => {
    Axios.get("products/new-arrivals")
      .then(res => {
        if (res.status == 200) {
          commit("setNewArrivals", res);
          resolve(true);
        }
      })
      .catch(err => {
        reject(err);
      });
  });
};

const getCategories = ({ commit }, payload) => {
  return new Promise((resolve, reject) => {
    Axios.get("products/categories")
      .then(res => {
        if (res.status == 200) {
          commit("setCategories", res);
          resolve(true);
        }
      })
      .catch(err => {
        reject(err);
      });
  });
};

const getSingleProduct = ({ commit }, payload) => {
  return new Promise((resolve, reject) => {
    Axios.get(`products/${payload.slug}`)
      .then(res => {
        if (res.status == 200) {
          commit("setSingleProduct", res.data);
          resolve(true);
        }
      })
      .catch(err => {
        reject(err);
      });
  });
};

export default {
  getNewArrivals,
  getAllProducts,
  getCategories,
  getSingleProduct
};
