const getNewArrivals = state => {
  return state.newArrivals;
};

const getAllProducts = state => {
  return state.allProducts.data;
};

const getAllProductsPagination = state => {
  return state.allProducts.pagination;
};

const getCategories = state => {
  return state.categories;
};

const getSingleProduct = state => {
  return state.singleProduct;
};

const searchProduct = state => {
  return state.searchProduct;
};

export default {
  getNewArrivals,
  getAllProductsPagination,
  getAllProducts,
  getCategories,
  getSingleProduct,
  searchProduct
};
