import actions from "./actions";
import getters from "./getters";
import mutations from "./mutations";

const state = {
  allProducts: {
    data: null,
    pagination: null
  },
  newArrivals: null,
  categories: null,
  singleProduct: null,

  searchProduct: null
};

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
};
