const getAccount = state => {
  return state.account;
};

export default {
  getAccount
};
