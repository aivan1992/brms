import Axios from "axios";

import { default_header, multipart_header } from "@/request_headers.js";

const updateAccount = ({ commit, state }, payload) => {
  return new Promise((resolve, reject) => {
    Axios.put(`account/${payload.id}`, state.account, default_header)
      .then(res => {
        resolve(true);
      })
      .catch(err => {
        reject(err);
      });
  });
};

const editAccount = ({ commit }, payload) => {
  return new Promise((resolve, reject) => {
    Axios.get(`account/${payload.id}`, default_header)
      .then(res => {
        commit("setAccount", res.data);
        resolve(true);
      })
      .catch(err => {
        reject(err);
      });
  });
};

export default {
  editAccount,
  updateAccount
};
