const setAccount = (state, payload) => {
  state.account = payload;
};

export default {
  setAccount
};
