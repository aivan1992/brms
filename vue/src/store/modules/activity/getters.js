const getCartCount = state => {
  return state.cart_count;
};

export default {
  getCartCount
};
