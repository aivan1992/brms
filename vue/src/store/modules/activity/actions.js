import Axios from "axios";

const addToCart = ({ commit }, payload) => {
  return new Promise((resolve, reject) => {
    Axios.post("activities/add-to-cart", payload, vm.getOath2Token())
    .then(res => {
      if (res.status == 200) {
        resolve(res.data);
      }
    })
    .catch(err => {
      reject(err);
    });
  });
};

const addToFavorite = ({ commit }, payload) => {
  return new Promise((resolve, reject) => {
    Axios.post("activities/add-to-favorite", payload, vm.getOath2Token())
    .then(res => {
      if (res.status == 200) {
        resolve(res.data);
      }
    })
    .catch(err => {
      reject(err);
    });
  });
};

const getCartCount = ({ commit }, payload) => {
  return new Promise((resolve, reject) => {
    Axios.post("activities/get-cart-count", payload, vm.getOath2Token())
    .then(res => {
      if (res.status == 200) {
        commit("setCartCount", res.data);
        resolve(true);
      }
    })
    .catch(err => {
      reject(err);
    });
  });
};


const subscribe = ({ commit }, payload) => {
  return new Promise((resolve, reject) => {
    Axios.post("activities/subscribe", payload, vm.getOath2Token())
    .then(res => {
      if (res.status == 200) {
        resolve(true);
      } else {
        resolve(false);
      }
    })
    .catch(err => {
      reject(err.response.message);
    });
  });
};


const contact = ({ commit }, payload) => {
  return new Promise((resolve, reject) => {
    Axios.post("activities/contact", payload, vm.getOath2Token())
    .then(res => {
      if (res.status == 200) {
        resolve(true);
      }
    })
    .catch(err => {
      reject(err.response.message);
    });
  });
};



export default {
  addToCart,
  getCartCount,
  addToFavorite,
  subscribe,
  contact
};
