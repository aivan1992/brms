const setCartCount = (state, payload) => {
  state.cart_count = payload.count;
};

export default {
  setCartCount
};
