import Vue from "vue";
import App from "./App.vue";
import router from "./router";

import Axios from "axios";
// Axios.defaults.baseURL = "http://admin.yourboutique.ga/api";
Axios.defaults.baseURL = "http://127.0.0.1:8000/api";

import store from "./store";

import customPlugin from "./custom-plugins";
Vue.use(customPlugin);

import VueGoodTablePlugin from "vue-good-table";
Vue.use(VueGoodTablePlugin);
import "vue-good-table/dist/vue-good-table.css";

import "@/externalscript";

/*
 *  IMPORT THE GLOBAL MIXIN HELPERS
 */
import { mixin } from "@/mixins/index";
/*
 */

router.beforeEach((to, from, next) => {
  // to and from are both route objects. must call `next`.

  if (to.meta.noAuth) {
    if (localStorage.getItem("token")) {
      store.commit("displayAuth", false);
      next();
    } else {
      next();
    }
  } else {
    if (localStorage.getItem("token")) {
      store.commit("displayAuth", false);
      next();
    } else {
      store.commit("displayAuth", true);
      next({ name: "Home" });
    }
  }
});

import Swal from "sweetalert2";
window.Swal = Swal;

router.beforeEach((to, from, next) => {
  // This goes through the matched routes from last to first, finding the closest route with a title.
  // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
  const nearestWithTitle = to.matched
    .slice()
    .reverse()
    .find(r => r.meta && r.meta.title);

  // Find the nearest route element with meta tags.
  const nearestWithMeta = to.matched
    .slice()
    .reverse()
    .find(r => r.meta && r.meta.metaTags);
  const previousNearestWithMeta = from.matched
    .slice()
    .reverse()
    .find(r => r.meta && r.meta.metaTags);

  // If a route with a title was found, set the document (page) title to that value.
  if (nearestWithTitle) document.title = nearestWithTitle.meta.title;

  // Remove any stale meta tags from the document using the key attribute we set below.
  Array.from(document.querySelectorAll("[data-vue-router-controlled]")).map(
    el => el.parentNode.removeChild(el)
  );

  // Skip rendering meta tags if there are none.
  if (!nearestWithMeta) return next();

  // Turn the meta tag definitions into actual elements in the head.
  nearestWithMeta.meta.metaTags
    .map(tagDef => {
      const tag = document.createElement("meta");

      Object.keys(tagDef).forEach(key => {
        tag.setAttribute(key, tagDef[key]);
      });

      // We use this to track which meta tags we create, so we don't interfere with other ones.
      tag.setAttribute("data-vue-router-controlled", "");

      return tag;
    })
    // Add the meta tags to the document head.
    .forEach(tag => document.head.appendChild(tag));

  next();
});

Vue.directive("click-outside", {
  bind: function(el, binding, vnode) {
    window.event = function(event) {
      if (!(el == event.target || el.contains(event.target))) {
        vnode.context[binding.expression](event);
      }
    };
    document.body.addEventListener("click", window.event);
  },
  unbind: function(el) {
    document.body.removeEventListener("click", window.event);
  }
});

// Vue.prototype.can = function(value) {
//   return window.Laravel.permissions.includes(value);
// }

// Vue.prototype.is = function(value) {
//   return window.Laravel.role.includes(value);
// }

Vue.config.productionTip = false;

/**
 * Set Global variable vm
 *
 */

var app = new Vue({
  mixins: [mixin],
  router,
  store,
  render: h => h(App)
}).$mount("#app");

global.vm = app;
