const customPlugin = {
  install(Vue, options) {
    Vue.mixin({
      mixins: () => "../mixins"
    });
  }
};

export default customPlugin;
