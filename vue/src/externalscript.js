import Vue from "vue";

import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

// import BootstrapVue from 'bootstrap-vue'
// Vue.use(BootstrapVue)
// import 'bootstrap-vue/dist/bootstrap-vue.css';
import Popper from "popper.js";
global.Popper = Popper;

window.moment = require("moment");

window.jQuery = require("jquery");
window.$ = require("jquery");

require("@/assets/plugins/OwlCarousel2-2.2.1/owl.carousel.js");

require("@/assets/plugins/easing/easing.js");
require("@/assets/js/custom.js");

require("@/assets/js/contact_custom.js");
require("@/assets/js/single_custom.js");
