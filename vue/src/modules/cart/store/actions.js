import Axios from "axios";
import Vue from "vue";

const requestHeader = () => {
  return {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token")
    }
  };
};

const submitRentalRequest = ({ commit, state }, payload) => {
  return new Promise((resolve, reject) => {
    Axios.post("transactions/submit-request", payload, requestHeader()).then(
      res => {
        resolve(res.data);
      }
    );
  });
};

const getCartProducts = ({ commit, state }, payload) => {
  return new Promise((resolve, reject) => {
    Axios.post("transactions/get-cart-products", payload, requestHeader()).then(
      res => {
        commit("setCartProducts", res.data);
        resolve(true);
      }
    );
  });
};

const removeToCart = ({ commit, state }, payload) => {
  return new Promise((resolve, reject) => {
    Axios.post("transactions/remove-to-cart", payload, requestHeader()).then(
      res => {
        resolve(res.data);
      }
    );
  });
};

export default {
  submitRentalRequest,
  getCartProducts,
  submitRentalRequest,
  removeToCart
};
