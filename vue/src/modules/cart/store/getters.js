const getCartProducts = state => {
  return state.products;
};

export default {
  getCartProducts
};
