const setCartProducts = (state, payload) => {
  state.products = payload.products;
};

const filterReqDays = (state, payload) => {
  state.products[payload.index].requested_days = payload.value;
};

export default {
  setCartProducts,
  filterReqDays
};
