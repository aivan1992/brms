import Axios from "axios";
import Vue from "vue";

const loginUser = ({ commit, state }, payload) => {
  return new Promise((resolve, reject) => {
    Axios.post("login", {
      email: state.email,
      password: state.password
    })
      .then(response => {
        if (response.status === 200) {
          state.Authentication = response.data.success.token;

          localStorage.setItem("token", state.Authentication);
          localStorage.setItem("role", response.data.success.role);
          localStorage.setItem("name", response.data.success.name);
          localStorage.setItem("email", response.data.success.email);
          localStorage.setItem("user_id", response.data.success.user_id);

          localStorage.setItem(
            "permissions",
            JSON.stringify(response.data.success.permissions)
          );
          commit("setAuth");
          resolve(true);
        }
      })
      .catch(err => {
        reject(err);
      });
  });
};

const logoutUser = state => {
  return new Promise((resolve, reject) => {
    // state.token = "";
    localStorage.clear();
    resolve(true);
  });
};

const registerUser = ({ commit, state }, payload) => {
  return new Promise((resolve, reject) => {
    Axios.post("register", {
      email: state.email,
      password: state.password,
      name: state.name,
      permissions: ["view"]
    })
      .then(response => {
        if (response.status == 201) {
          state.Authentication = response.data.success.token;
          localStorage.setItem("token", state.Authentication);
          localStorage.setItem("role", response.data.success.role);
          localStorage.setItem("name", response.data.success.name);
          localStorage.setItem("email", response.data.success.email);
          localStorage.setItem(
            "permissions",
            JSON.stringify(response.data.success.permissions)
          );
          commit("setAuth");
          resolve(true);
        }
      })
      .catch(err => {
        console.log(err);
        reject(err);
      });
  });
};

export default {
  loginUser,
  registerUser,
  logoutUser
};
