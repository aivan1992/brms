export default {
	data() {
		return {
			isMobile: false,
			windowWidth: window.innerWidth,
		}
	},

	methods: {
		onResize() {
			this.windowWidth = window.innerWidth
		}
	},

	mounted() {

		if(window.innerWidth > 700) {
			this.isMobile = false;
		} else {
			this.isMobile = true;
		}

		this.$nextTick(() => {
			window.addEventListener('resize', this.onResize);
		})
	},

	beforeDestroy() { 
		window.removeEventListener('resize', this.onResize); 
	},

	watch: {
		windowWidth(value) {
			if(+value > 700) {
				return this.isMobile = false;
			} else {
				return this.isMobile = true;
			} 

		}
	}
}