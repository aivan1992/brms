<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BulkMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


     Schema::create('rental_requests_cancelled', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->timestamps();
    });


     Schema::create('transaction_on_going', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->timestamps();
    });


     Schema::create('transactions_completed', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->timestamps();
    });


     Schema::create('transactions_void_refunded', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->timestamps();
    });


     Schema::create('images', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->timestamps();

        $table->string('file_name')->nullable();
        $table->string('directory_name')->nullable();

        $table->integer('imageable_id')->nullable();
        $table->string('imageable_type')->nullable();

    });


     Schema::create('categories', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->timestamps();

        $table->string('name')->nullable();
        $table->text('slug');
    });


     Schema::create('transactions', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->timestamps();

        $table->tinyInteger('generated')->nullable();

            /**
            * generated column is used to be the temporary fillable to create unique ID 
            *
            */
        });



     Schema::create('rentals', function (Blueprint $table) {


      $table->bigIncrements('id');
      $table->timestamps();

      $table->integer('customer_id')->nullable();

      $table->string('status')->nullable();
          /**
             *  Status posible values
            *  void-refunded - Cancelled - completed - pending
            *   ongoing 
             *  
             */ 

          $table->integer('transaction_id')->nullable();
          $table->integer('product_id')->nullable();

          $table->integer('requested_days')->nullable();
          $table->integer('approved_days')->nullable();
          $table->date('date_start')->nullable();
          $table->date('date_end')->nullable();
          $table->integer('penalty_days')->nullable();
          $table->decimal('total_rent_amount',  15, 2)->nullable();

          $table->decimal('penalty_amount', 15, 2)->nullable();
          $table->string('payment_status')->nullable();
          $table->dateTime('item_received_date')->nullable();
          $table->dateTime('cancelled')->nullable();
          /**
             *  payment_status posible values
            *  paid - void-refunded - pending 
            *  
             *  
             */
      });


 }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
