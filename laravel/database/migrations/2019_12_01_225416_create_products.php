<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            
            $table->string('title')->nulllable();
            $table->string('serial')->nulllable();

            $table->text('desciption')->nullable();
            $table->text('slug');

            // *the listed below are the relation related to products* 
            //categories
            //image   

            $table->decimal('daily_rate', 15,2)->default(0);

            $table->tinyInteger('status')->nullable();
            /**
            * The possible value of status are
            *
            * 1 for available, 0 for not available 
            */
            $table->integer('category_id')->nullable();
            


            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
