<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();


            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('province')->nullable();
            $table->text('address')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('town_city')->nullable();
            $table->string('country')->nullable();

            $table->string('zip', 20)->nullable();
            $table->bigInteger('user_id');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }




}
