<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [ 'casual','accessories', 'formal'];
        

        foreach ($categories as $category) {

            if(!Category::whereName($category)->exists()) {
                Category::create(['name' => $category]);
            }

        }
    }
}
