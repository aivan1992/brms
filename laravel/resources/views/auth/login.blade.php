@extends('template.auth')



@section('form')


<form method="POST" action="{{ route('login') }}">
    @csrf
    <!-- <a href="/" style="color: black"><h2 class="text-center mb-3">YOUR BOTIQUE</h2></a> -->
    <div class="col-lg-9 mx-auto">
         <a href="/">
    <img src="{{asset('assets/images/logo-your-boutique.png')}}" class="w-100">
</a>
    </div>

    <div class="form-group">
        <label class="label">Email</label>
        <input  type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : ''}} form-control-lg" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
        @if ($errors->has('email'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
        @endif
    </div>


    <div class="form-group">
        <label class="label">Password</label>
        <input id="password" type="password" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-lg" name="password" required autocomplete="current-password">
        @if ($errors->has('email'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
        @endif
    </div>

    <div class="form-group">
        <button class="btn btn-primary submit-btn btn-block">Login</button>
    </div>

    <!-- <div class="form-group d-flex justify-content-between"> -->
    <!-- <div class="form-check form-check-flat mt-0">
      <label class="form-check-label">
        <input type="checkbox" class="form-check-input" checked> Keep me signed in </label>
      </div>
      <a href="#" class="text-small forgot-password text-black">Forgot Password</a>
  </div> -->
  <div class="mb-5">
   {{-- <a href="{{ route('password.request') }}" class="pull-right text-small forgot-password text-black">Forgot Password</a> --}}
</div>

<!--  <div class="form-group">
  <button class="btn btn-block g-login">
    <img class="mr-3" src="{{asset('assets/images/file-icons/icon-google.svg')}}" alt="">Log in with Google</button>
</div> -->
<div class="text-block text-center my-3">
    {{-- <span class="text-small font-weight-semibold">Not a member ?</span>
    <a href="/register" class="text-black text-small">Create new account</a> --}}
</div>
</form>















@endsection
