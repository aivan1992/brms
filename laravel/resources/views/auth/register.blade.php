@extends('template.auth')

@section('form')


<form method="POST" action="{{ route('register') }}">
  @csrf


 <div class="col-lg-5 mx-auto">
  <a href="/">
    <img src="{{asset('assets/images/your-boutique.png')}}" class="w-100">
    </div>
</a>
 <div class="form-group">
    <label class="label">Name</label>
        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} form-control-lg" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus >
      
  @if ($errors->has('name'))
  <span class="invalid-feedback" role="alert">
    <strong>{{ $errors->first('name') }}</strong>
</span>
@endif
</div>




<div class="form-group">
    <label class="label">Email</label>
        <input  type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-lg" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus >

  @if ($errors->has('email'))
  <span class="invalid-feedback" role="alert">
    <strong>{{ $errors->first('email') }}</strong>
</span>
@endif
</div>



<div class="form-group">
    <label class="label">Password</label>
     <input  type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-lg" name="password" required autocomplete="new-password" placeholder="Password">
@if ($errors->has('password'))
<span class="invalid-feedback" role="alert">
    <strong>{{ $errors->first('password') }}</strong>
</span>
@endif
</div>

<div class="form-group">
    <label class="label">Confirm Password</label>
    
        <input type="password" name="password_confirmation" class="form-control form-control-lg" required autocomplete="new-password" placeholder="Password">
</div>

<div class="form-group d-flex justify-content-center">
    <div class="form-check form-check-flat mt-0">
      <label class="form-check-label">
        <input type="checkbox" class="form-check-input" required> I agree to the terms </label>
    </div>
</div>

<div class="form-group">
  <button class="btn btn-primary submit-btn btn-block" type="submit">Register</button>
</div>
<div class="text-block text-center my-3">
  <span class="text-small font-weight-semibold">Already have and account ?</span>
  <a href="{{route('login')}}" class="text-black text-small">Login</a>
</div>
</form>

@endsection


