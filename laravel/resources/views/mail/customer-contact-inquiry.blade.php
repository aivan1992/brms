
@component('mail::layout')

@slot('header')
@component('mail::header',['url' => ''])
<span class="bg-danger">YOUR BOUTIQUE</span> 
@endcomponent
@endslot

#Hello, greetings admin<br>
A customer contacted you for inquiry. the message information is listed below.
<br>

#Name: {{$info['name']}}
#Email: {{$info['email']}}
#Contact #: {{$info['contact_number']}}
#Message: {{$info['message']}}


@slot('footer')
@component('mail::footer')
Copyright {{ now()->year }} YOUR BOUTIQUE
@endcomponent
@endslot

@endcomponent
