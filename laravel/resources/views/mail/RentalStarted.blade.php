
@component('mail::layout')

  @slot('header')
        @component('mail::header',['url' => ''])
           <span class="bg-danger">YOUR BOUTIQUE</span> 
        @endcomponent
    @endslot

#Hello, <br>
Your rental with the transaction ID of {{$rental->transaction_id}} is approved, its duration is {{$rental->rentalStartEnd}}. <br>
<br>
Please return the item before or during the end date. Thank you. 

   @slot('footer')
        @component('mail::footer')
            Copyright {{ now()->year }} YOUR BOUTIQUE
        @endcomponent
    @endslot

@endcomponent
