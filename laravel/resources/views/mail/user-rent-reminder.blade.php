
@component('mail::layout')

  @slot('header')
        @component('mail::header',['url' => ''])
           <span class="bg-danger">YOUR BOUTIQUE</span> 
        @endcomponent
    @endslot

#Hello, <br>
This is to remind you that your rental will be expiring at  
{{$rental->endDateMutated}}.
<br>
#Product name: {{$rental->product()->value('title')}}
#Transaction ID: {{$rental->transaction_id}}
#Rental Duration: {{$rental->rentalStartEnd}}

   @slot('footer')
        @component('mail::footer')
            Copyright {{ now()->year }} YOUR BOUTIQUE
        @endcomponent
    @endslot

@endcomponent
