  @extends('template.main')

  @section('body')

  @include('partials.card-header', ['title' => 'Rental Accepted Request'])


  <div class="col-lg-5">
   <div class="form-group">
    <div class="input-group">
      <input type="text" class="form-control" style="font-size: 1.2em" placeholder="Search" aria-label="Search" aria-describedby="colored-addon3">
      <div class="input-group-append bg-secondary border-primary">
        <span class="input-group-text bg-transparent">
          <i class="fa fa-search "></i>
        </span>
      </div>
    </div>
  </div>

</div>

<div class="col-lg-12 grid-margin stretch-card">
 <div class="card">
  <div class="card-body">
   <div class="table-responsive">
     <table class="table table-hover">
      <thead>
       <tr>
        <th>RENT ID</th>
        <th>DATE ACCEPTED</th>
        <th>IDENTIFIERS</th>
        <th>PARTIAL AMOUNT</th>
        <th>REQUESTED DAYS</th>
         <th>REQUESTED QTY</th>
         <th>APPROVED QTY</th>
        <th>DATE START / END</th>
        <th>MOVE / EDIT</th>
        <th>CANCEL REQUEST</th>
      </tr>
    </thead>
    <tbody>

      @foreach($rentals as $rental)
      <tr>
        <td>{{$rental->id}}</td>
        <td>{{$rental->approvedAtMutated}}</td>
        <td>
          @include('partials.transanction-user-product', [
          'transaction_id' => $rental->transaction_id,
          'customer_id' => $rental->customer_id,
          'product_id' => $rental->product_id
          ])
        </td>

        <td class="text-primary">{{$rental->totalRentAmountMutated}}</td>
        <td>{{$rental->requested_days}}</td>
       <td>{{$rental->qty}}</td>
       <td>

        <a href="Javascript:void(0)" onclick="showSetQtyModal(
          '{{$rental->id}}',
          '{{$rental->approved_qty}}'
          )" class="custom-link-text">{{$rental->approved_qty}}
           </a>

         </td>
       
        <td>
          <a href="Javascript:void(0)" onclick="showDateModal(
          '{{$rental->id}}',
          '{{$rental->startDateMutated}}',
          '{{$rental->endDateMutated}}'
          )" class="custom-link-text">{{$rental->rentalStartEnd}}
           </a>
        </td>
          
         <td>
          <ul class="list-group custom-lists-horizontal"> 
            <li >
              <button type="button" title="move to on-going" onclick="updateStatus('on-going', '{{$rental->id}}')" class="btn social-btn btn-inverse-secondary">
                <i class="fa fa-mail-forward
                text-black"></i>
              </li>
              <li>
               <button type="button"  title="update date" class="btn social-btn btn-inverse-secondary">
                <i class="text-black fa fa-edit"></i>
              </button>
            </li>
          </ul>
        </td>
        <td>
         <button type="button"  title="cancel request"  onclick="updateStatus('cancelled', '{{$rental->id}}')" class="btn social-btn btn-inverse-secondary">
          <i class="text-black fa fa-ban"></i>
        </button>
      </td>
    </tr>
    @endforeach
  </tbody>
  <tfoot>

  <tr>
    <td colspan="12">
      <div class="text-right pull-right">

        {{ $rentals->links() }}

      </div>
    </td>
  </tr>
</tfoot>
</table>
</div>
</div>
</div>
</div>



<!-- Modal -->
<div class="modal fade" id="rent_start_end">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">RENT START DATE & END</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-lg-12">
            <div class="form-group">
              <label >Rental ID</label>
              <input type="text" class="form-control border-0" id="rental_id" readonly="">
            </div>
          </div>
        </div>  

        <div class="row">
          <div class="col-lg-12">
            <div class="form-group">
              <label >Start Date</label>
              <input type="date" class="form-control" id="date_start" name="date_start">
            </div>
          </div>
        </div>    
        <div class="row">
          <div class="col-lg-12">
            <div class="form-group">
              <label >End Date</label>
              <input type="date" class="form-control" id="date_end" name="date_end">
            </div>
          </div>
        </div>    


      </div>
      <div class="modal-footer">
       <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       <button type="button" onclick="updateDateStartEnd()" class="btn btn-primary">Submit</button>
     </div>
   </div>
 </div>
</div>


<!-- Modal -->
<div class="modal fade" id="appqty">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">SET RENTAL QTY</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-lg-12">
            <div class="form-group">
              <label >Rental ID</label>
              <input type="text" class="form-control border-0" id="p_rental_id" readonly="">
            </div>
          </div>
        </div>  

        <div class="row">
          <div class="col-lg-12">
            <div class="form-group">
              <label>QTY</label>
              <input type="number" class="form-control" id="qty" name="qty">
            </div>
          </div>
        </div>    
      

      </div>
      <div class="modal-footer">
       <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       <button type="button" onclick="updateApprovedQty()" class="btn btn-primary">Submit</button>
     </div>
   </div>
 </div>
</div>




<script>


  var showDateModal = (id, start, end) => {
   document.querySelector('#rental_id').value = id
   if(start !== '' && end !== '') {
    document.querySelector('#date_start').value = start
    document.querySelector('#date_end').value = end  
  } else {
    document.querySelector('#date_start').value 
      // TO DO
      // set date now when start date is empty
    }

    $('#rent_start_end').modal();

  }


  var showSetQtyModal = (id, qty) => {
   document.querySelector('#p_rental_id').value = id
   document.querySelector('#qty').value = qty

    $('#appqty').modal();

  }







  const updateDateStartEnd = (e) => {

    // if(confirm('Are you sure you want to proceed')) {
     $.ajaxSetup({
      headers: { 'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content }
    })

     $.ajax({
      url: '/rental-requests/update-start-end',
      method: 'POST',
      data: { 
        id: document.querySelector('#rental_id').value,
        date_start:  document.querySelector('#date_start').value,
        date_end:  document.querySelector('#date_end').value
      },
      success:  res => {
        alert(JSON.stringify(res.success))
        window.location.reload();
      },  
      catch : err => {
       alert(JSON.stringify(err))
     }})
   }

 // } 



  const updateApprovedQty = (e) => {

    // if(confirm('Are you sure you want to proceed')) {
     $.ajaxSetup({
      headers: { 'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content }
    })

     $.ajax({
      url: '/rental-requests/update-approved-qty',
      method: 'POST',
      data: { 
        id: document.querySelector('#p_rental_id').value,
        qty:  document.querySelector('#qty').value,
      },
      success:  res => {
        alert(JSON.stringify(res.success))
        window.location.reload();
      },  
      catch : err => {
       alert(JSON.stringify(err))
     }})
   }










 const updateStatus = (value, id) => {

  if(confirm('Are you sure you want to proceed')) {
   $.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content }
  })


   $.ajax({
    url: '/rental-requests/status',
    method: 'POST',
    data: { 
     id,
     status: value
   },
   success:  res => {
    alert(JSON.stringify(res.success))
    window.location.reload();
  },  
  catch : err => {
   alert(JSON.stringify(err))
 }})
 }

} 


</script>

@endsection