  @extends('template.main')

  @section('body')

  @include('partials.card-header', ['title' => 'Customer Cancel Requests'])



  <div class="col-lg-5">
   <div class="form-group">
    <div class="input-group">
      <input type="text" class="form-control" style="font-size: 1.2em" placeholder="Search" aria-label="Search" aria-describedby="colored-addon3">
      <div class="input-group-append bg-secondary border-primary">
        <span class="input-group-text bg-transparent">
          <i class="fa fa-search "></i>
        </span>
      </div>
    </div>
  </div>
</div>

<div class="col-lg-12 grid-margin stretch-card">
 <div class="card">
  <div class="card-body">
   <div class="table-responsive">
     <table class="table table-hover">
      <thead>
       <tr>
        <th>RENT ID</th>
        <th>IDENTIFIERS</th>
        <th>PARTIAL AMOUNT</th>
        <th>REQUESTED DAYS</th>
         <th>REQUESTED QTY</th>
        <th>STATUS</th>
        <th>ACCEPT / REJECT REQUEST</th>
      </tr>
    </thead>
    <tbody>

      @foreach($rentals as $rental)
      <tr>
        <td>{{$rental->id}}</td>
        <td>
         @include('partials.transanction-user-product', [
         'transaction_id' => $rental->transaction_id,
         'customer_id' => $rental->customer_id,
         'product_id' => $rental->product_id
         ])
       </td>
       <td class="text-primary">{{$rental->totalRentAmountMutated}}</td>
       <td>{{$rental->requested_days}}</td>
       <td>{{$rental->qty}}</td>
       
       <td>
         <label class="badge badge-success custom-min-width-label">{{$rental->status}}</label>
       </td>
       <td>
        <ul class="list-group custom-lists-horizontal"> 
          <li >
            <button type="button" title="accept request" onclick="updateStatus('cancelled', '{{$rental->id}}')" class="btn social-btn btn-inverse-secondary">
              <i class="fa fa-check text-black"></i>
            </button>
          </li>
          <li>
           <button type="button"  title="reject request" 
            onclick="denyCancelRequest('{{$rental->customer_id}}', '{{$rental->id}}')" class="btn social-btn btn-inverse-secondary">
            <i class="text-black fa fa-ban"></i>
          </button>
        </li>
      </ul>
    </td>
  </tr>
  @endforeach
</tbody>
<tfoot>

  <tr>
    <td colspan="12">
      <div class="text-right pull-right">

        {{ $rentals->links() }}

      </div>
    </td>
  </tr>
</tfoot>

</table>
</div>
</div>
</div>
</div>


<script>

 const updateStatus = (value, id) => {

  if(confirm('Are you sure you want to proceed')) {
   $.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content }
  })

   $.ajax({
    url: '/rental-requests/status',
    method: 'POST',
    data: { 
     id,
     status: value
   },
   success:  res => {
    alert(JSON.stringify(res.success))
    window.location.reload();
  },  
  catch : err => {
   alert(JSON.stringify(err))
 }})
 }

} 



 const denyCancelRequest = (user_id, rental_id) => {

  if(confirm('Are you sure you want to proceed')) {
   $.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content }
  })

   $.ajax({
    url: '/transactions/deny-request',
    method: 'POST',
    data: { 
     user_id,
     rental_id
   },
   success:  res => {
    alert(JSON.stringify(res.success))
    window.location.reload();
  },  
  catch : err => {
   alert(JSON.stringify(err))
 }})
 }

} 



</script>

@endsection