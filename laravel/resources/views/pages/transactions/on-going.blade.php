  @extends('template.main')

  @section('body')

  @include('partials.card-header', ['title' => 'On Going Transaction'])


  <div class="col-lg-5">
   <div class="form-group">
    <div class="input-group">
      <input type="text" class="form-control" style="font-size: 1.2em" placeholder="Search" aria-label="Search" aria-describedby="colored-addon3">
      <div class="input-group-append bg-secondary border-primary">
        <span class="input-group-text bg-transparent">
          <i class="fa fa-search "></i>
        </span>
      </div>
    </div>
  </div>
</div>

<div class="col-lg-12 grid-margin stretch-card">
 <div class="card">
  <div class="card-body">
    <div class="table-responsive">
     <table class="table table-hover">
      <thead>
       <tr>
        <th>RENT ID</th>
        <th>IDENTIFIERS</th>
        <th>TOTAL AMOUNT</th>
        <th>RENT START & END</th>
        <th>DAYS</th>
        <th>APPROVED QTY</th>

    
        <th>PENALTY</th>
        <th>RECEIVED AT</th>
        <th>COMPLETED / VOID</th>
      </tr>
    </thead>
    <tbody>

      @foreach($rentals as $rental)
      <tr>
        <td>{{$rental->id}}</td>
        <td> 
          @include('partials.transanction-user-product', [
          'transaction_id' => $rental->transaction_id,
          'customer_id' => $rental->customer_id,
          'product_id' => $rental->product_id
          ])
        </td>
        <td class="text-primary">{{$rental->totalRentAmountMutated}}</td>
        <td><a href="Javascript:void(0)" class="custom-link-text" onclick="showRentalStartEndModal('{{$rental->date_start}}', '{{$rental->date_end}}', '{{$rental->id}}')">{{$rental->rentalStartEnd}} </a></td>
        <td>{{$rental->daysLeft}}</td>
      <td>{{$rental->approved_qty}}</td>

        <td>{{$rental->penaltyAmountMutated}}</td>
        <td>{{$rental->dateTimeReceived}}</td>
        
       <td>
        <ul class="list-group custom-lists-horizontal"> 
          <li>
           <button type="button"  title="mark as completed"  onclick="updateStatus('completed', '{{$rental->id}}')" class="btn social-btn btn-inverse-secondary">
            <i class="text-black fa fa-check"></i>
          </button>
        </li>
        <li >
          <button type="button" title="void/refund transaction" onclick="updateStatus('void-refunded', '{{$rental->id}}')" class="btn social-btn btn-inverse-secondary">
            <i class="fa fa-ban text-black"></i>
          </li>
        </ul>
      </td>
    </tr>
    @endforeach
  </tbody>
  <tfoot>

  <tr>
    <td colspan="12">
      <div class="text-right pull-right">

        {{ $rentals->links() }}

      </div>
    </td>
  </tr>
</tfoot>
</table>

</div>

</div>
</div>
</div>

<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#rent_start_end">
  Launch demo modal
</button> -->

<div class="modal fade" id="rent_start_end">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">RENT START DATE & END</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-12">
            <div class="form-group">
              <label >Start Date</label>
              <input type="date" class="form-control" id="date_start" name="date_start">
            </div>
          </div>
        </div>    
        <div class="row">
          <div class="col-lg-12">
            <div class="form-group">
              <label >End Date</label>
              <input type="date" class="form-control" id="date_end" name="date_end">
            </div>
          </div>
        </div>    
        <input type="hidden" id="rental_id" name="id">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="submitRentStartEndModal()">Submit</button>
      </div>
    </div>
  </div>
</div>



<script>

  const updateStatus = (value, id) => {

    if(confirm('Are you sure you want to proceed')) {
     $.ajaxSetup({
      headers: { 'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content }
    })

     $.ajax({
      url: '/rental-requests/status',
      method: 'POST',
      data: { 
       id,
       status: value
     },
     success:  res => {
      alert(JSON.stringify(res.success))
      window.location.reload();
    },  
    catch : err => {
     alert(JSON.stringify(err))
   }})
   }

 } 


 const showRentalStartEndModal = (start, end, id) => {

  if(confirm('Are you sure you want to proceed')) {
    document.querySelector('#date_start').value = start;
    document.querySelector('#date_end').value = end;
    document.querySelector('#rental_id').value = id;
    $('#rent_start_end').modal({show: true});
  }

}


const submitRentStartEndModal = () => {
  let start = document.querySelector('#date_start').value ;
  let end = document.querySelector('#date_end').value;
  let id = document.querySelector('#rental_id').value;

  $.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content }
  })
  $.ajax({
    url: '/transactions/update-start-end',
    method: 'POST',
    data: { 
     id,
     date_start: start,
     date_end: end
   },
   success:  res => {
    alert(JSON.stringify(res.success))
    window.location.reload();
  },  
  catch : err => {
   alert(JSON.stringify(err))
 }})
}



</script>

@endsection