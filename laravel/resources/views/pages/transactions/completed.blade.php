  @extends('template.main')

  @section('body')

  @include('partials.card-header', ['title' => 'Completed Transaction'])



  <div class="col-lg-5">

  	<div class="form-group">
  		<div class="input-group">
  			<input type="text" class="form-control" style="font-size: 1.2em" placeholder="Search" aria-label="Search" aria-describedby="colored-addon3">
  			<div class="input-group-append bg-secondary border-primary">
  				<span class="input-group-text bg-transparent">
  					<i class="fa fa-search "></i>
  				</span>
  			</div>
  		</div>
  	</div>


  </div>

  <div class="col-lg-12 grid-margin stretch-card">
  	<div class="card">
  		<div class="card-body">
        <div class="table-responsive">
         <table class="table table-hover">
          <thead>
           <tr>
            <th>RENT ID</th>
            <th>DATE COMPLETED</th> 
            <th>IDENTIFIERS</th>
            <th>RENT START & END</th>
            <th>RECEIVED DATE</th>
            <th>PENALTY</th>
            <th>APPROVED DAYS</th>
            <th>RENT AMOUNT</th>
            <th>VOID TRANSACTION</th>
          </tr>
        </thead>
        <tbody>
          @foreach($rentals as $rental)
          <tr>
            <td>{{$rental->id}}</td>
            <td>{{$rental->completedAtMutated}}</td>
            <td>
              @include('partials.transanction-user-product', [
                'transaction_id' => $rental->transaction_id,
                'customer_id' => $rental->customer_id,
                'product_id' => $rental->product_id
                ])
              </td>


              <td>{{$rental->rentalStartEnd}} </td>
              <td>{{$rental->itemReceivedDateMutated}}</td>

              <td>{{$rental->penaltyAmountMutated}}</td>
              <td>{{$rental->approved_qty}}</td>
              
              <td class="text-primary">{{$rental->totalRentAmountMutated}}</td>

              <td>
                <button type="button" title="void/refund transaction" onclick="updateStatus('void-refunded', '{{$rental->id}}')" class="btn social-btn btn-inverse-secondary">
                  <i class="fa fa-ban text-black"></i>
                </button>
              </td>
            </tr>
            @endforeach
          </tbody>
          <tfoot>

            <tr>
              <td colspan="12">
                <div class="text-right pull-right">

                  {{ $rentals->links() }}

                </div>
              </td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>


<script>

  const updateStatus = (value, id) => {

    if(confirm('Are you sure you want to proceed')) {
     $.ajaxSetup({
      headers: { 'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content }
    })


     $.ajax({
      url: '/rental-requests/status',
      method: 'POST',
      data: { 
       id,
       status: value
     },
     success:  res => {
      alert(JSON.stringify(res.success))
      window.location.reload();
    },  
    catch : err => {
     alert(JSON.stringify(err))
   }})
   }

 } 
 const viewCustomerDetails = id  => {
  alert(id)
}

</script>

@endsection