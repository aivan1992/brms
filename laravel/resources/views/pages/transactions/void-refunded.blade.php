  @extends('template.main')

  @section('body')


  @include('partials.card-header', ['title' => 'Void / Refunded Transactions'])




  <div class="col-lg-5">

  	<div class="form-group">
  		<div class="input-group">
  			<input type="text" class="form-control" style="font-size: 1.2em" placeholder="Search" aria-label="Search" aria-describedby="colored-addon3">
  			<div class="input-group-append bg-secondary border-primary">
  				<span class="input-group-text bg-transparent">
  					<i class="fa fa-search "></i>
  				</span>
  			</div>
  		</div>
  	</div>


  </div>

  <div class="col-lg-12 grid-margin stretch-card">
  	<div class="card">
  		<div class="card-body">
  			<table class="table table-hover">
  				<thead>
  					<tr>
              <th>RENT ID</th>
              <th>VOIDED-REFUNDED AT</th>
              <th>IDENTIFIERS</th>
            </tr>
          </thead>
          <tbody>

            @foreach($rentals as $rental)
            <tr>
              <td>{{$rental->id}}</td>
                  <td> <label class="badge badge-success custom-min-width-label">{{$rental->voidRefundedMutated}}</label></td>
              <td> 
                @include('partials.transanction-user-product', [
                'transaction_id' => $rental->transaction_id,
                'customer_id' => $rental->customer_id,
                'product_id' => $rental->product_id
                ])
              </td>
          
            </tr>
            @endforeach

          </tbody>
          <tfoot>

  <tr>
    <td colspan="12">
      <div class="text-right pull-right">

        {{ $rentals->links() }}

      </div>
    </td>
  </tr>
</tfoot>
        </table>
      </div>
    </div>
  </div>


  <script>

  </script>

  @endsection