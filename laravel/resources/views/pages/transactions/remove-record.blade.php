  @extends('template.main')

  @section('body')

  @include('partials.card-header', ['title' => 'Rental Search / Remove Request'])



  <div class="col-lg-5">
   <div class="form-group">
    <div class="input-group">
      <input type="text" class="form-control" style="font-size: 1.2em" placeholder="Search" aria-label="Search" aria-describedby="colored-addon3">
      <div class="input-group-append bg-secondary border-primary">
        <span class="input-group-text bg-transparent">
          <i class="fa fa-search "></i>
        </span>
      </div>
    </div>
  </div>


</div>

<div class="col-lg-12 grid-margin stretch-card">
 <div class="card">
  <div class="card-body">
    <div class="row mb-1">
      <code class="text-center w-100">Warning removed row will be permanently deleted on database.</code>
    </div>
    <div class="table-responsive">
     <table class="table table-hover">
      <thead>
       <tr>
        <th>RENT ID</th>
        <th>IDENTIFIERS</th>
        <th>TABLE CATEGORY</th>
        <th>REMOVE RECORD</th>
      </tr>
    </thead>
    <tbody>

      @foreach($rentals as $rental)
      <tr>
        <td>{{$rental->id}}
        </td>
        <td> 
          @include('partials.transanction-user-product', [
          'transaction_id' => $rental->transaction_id,
          'customer_id' => $rental->customer_id,
          'product_id' => $rental->product_id
          ])
        </td>



        <td>
          <label class="badge badge-success custom-min-width-label ">
            <span class="">
              {{strtoupper($rental->status)}} 
            </span>
          </label>
        </td>

        <td>
         <button type="button" title="remove record" onclick="removeRecord('{{$rental->id}}')" class="btn social-btn btn-inverse-secondary">
          <i class="fa fa-trash-o text-black"></i>
        </button>
      </td>



    </tr>
    @endforeach

  </tbody>
  <tfoot>

    <tr>
      <td colspan="12">
        <div class="text-right pull-right">

          {{ $rentals->links() }}

        </div>
      </td>
    </tr>
  </tfoot>
</table>
</div>
</div>
</div>
</div>


<script>

 const removeRecord = (id) => {

  if(confirm('Are you sure you want to proceed')) {
   $.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content }
  })

   $.ajax({
    url: '/rental-requests/remove-record',
    method: "POST",
    data: {
     id 
    },
    success:  res => {
      alert(JSON.stringify(res.success))
      window.location.reload();
    },  
    catch : err => {
     alert(JSON.stringify(err))
   }})
 }

} 
</script>

@endsection