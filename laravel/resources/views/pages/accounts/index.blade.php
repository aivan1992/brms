  @extends('template.main')

  @section('body')

  @include('partials.card-header', ['title' => 'Account Lists'])

  @include('partials.success-error-notification')


  @include('partials.search-form', [ 'route' => '/accounts/search', 
  'searchKey' => [
   ['value' => 'email', 'text' => 'EMAIL'],
   ['value' => 'id', 'text' => 'USER ID'],
   ['value' => 'name', 'text' => 'USER NAME']
  ]])



  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>USER ID <a href="Javascript:void(0)" title="Click customer ID to view details"><i class="fa fa fa-id-badge text-primary"></i></a></th>
              <th>EMAIL</th>
              <th>USER NAME</th>
              <th>USER ROLE</th>
              <!-- <th>STATUS</th> -->
              <th>EDIT / DEACTIVATE</th>
              <th>SUSPEND / REMOVE ACCOUNT</th>
            </tr>
          </thead>
          <tbody>

            @foreach($accounts as $account)
            <tr>
              <td><a href="{{route('accounts.show', $account->id)}}" target="_blank" class="custom-link-text">{{$account->id}}</a></td>
              <td>{{$account->email}}</td>
              <td>{{$account->name}}</td>
              <td>
                <label class="badge badge-success custom-min-width-label">{{$account->getRoleNames()->first()}}</label>
              </td>
          <!--     <td>
                <label class="badge badge-success custom-min-width-label">pppp</label>
              </td> -->
              <td>
               <ul class="list-group custom-lists-horizontal"> 
                <li>
                  <a title="Edit account" href="{{route('accounts.edit', $account->id)}}" class="btn social-btn btn-inverse-secondary">
                    <i class="fa fa-edit text-black"></i>
                  </a>
                </li>
                <li>
                  <button type="button" title="Cancel request" class="btn social-btn btn-inverse-secondary">
                    <i class="fa fa-unlock text-black"></i>
                  </button>

                </li>
              </ul>
            </td>

            <td>
              <ul class="list-group custom-lists-horizontal"> 
                <li >
                 <button type="button" title="suspend user" class="btn social-btn btn-inverse-secondary">
                  <i class="fa fa-gavel text-black"></i>
                </button>
              </li>
              <li>
                <button type="button" title="remove user" onclick="destroyUser('{{$account->id}}')" class="btn social-btn btn-inverse-secondary">
                  <i class="fa fa-trash-o text-black"></i>
                </button>

              </li>
            </ul>
          </td>


        </tr>
        @endforeach

      </tbody>
    </table>
    <div class="col-lg-12 my-3">
      <a href="/accounts/create" class="btn btn-primary btn-fw">Add Account</a>
    </div> 
  </div>
</div>
</div>



<script>

 const destroyUser = id => {

  if(confirm('Are you sure you want to proceed')) {
   $.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content }
  })
   $.ajax({
    url: '/accounts/'+id,
    method: 'DELETE',
    data: { 
     id
   },
   success:  res => {
    alert(JSON.stringify(res.success))
    window.location.reload();
  },  
  catch : err => {
   alert(JSON.stringify(err))
 }})
 }

} 
</script>

@endsection