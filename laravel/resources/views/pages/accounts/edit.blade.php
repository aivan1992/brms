@extends('template.main')

@section('body')

@include('partials.card-header', ['title' => 'Edit Account'])

@include('partials.success-error-notification')

<div class="row">
	<div class="col-lg-12 d-flex justify-center ">
		<div class="col-lg-6 grid-margin stretch-card offset-lg-3">
			<div class="card">
				<div class="card-body">

					<form class="forms-sample"  action="{{route('accounts.update', $account->id)}}" method="POST">
						@method('PUT')
						@csrf
						<div class="form-group">
							<label>Name</label>
							<input  type="text" value="{{$account->name}}" class="form-control" name="name">
						</div>
						<div class="form-group">
							<label>Email address</label>
							<input  type="email" class="form-control" value="{{$account->email}}" name="email">
						</div>
						<div class="form-group">
							<label>Password</label>
							<input type="password"  class="form-control" name="password">
						</div>
						<div class="form-group">
							<label>Image upload</label>
							<input type="file" name="img[]" class="file-upload-default">
							<div class="input-group col-xs-12">
								<input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
								<span class="input-group-append">
									<button class="file-upload-browse btn btn-secondary" type="button">Upload</button>
								</span>
							</div>
						</div>
						<div class="form-group">
							<label>User roles</label>
							<select class="form-control" name="role">
								<option value="super-admin" {{$account->getRoleNames()->first() == 'super-admin' ? 'selected':'' }}>Super-admin</option>
								<option value="admin" {{$account->getRoleNames()->first() == 'admin' ? 'selected':'' }}>Admin</option>
								<option value="customer" {{$account->getRoleNames()->first() == 'customer' ? 'selected':''}}>Customer</option>
							</select>
						</div>

						<div class="mt-3">
							<button type="submit" class="btn btn-primary mr-2">Submit</button>
							<a href="{{route('accounts.index')}}" class="btn btn-light">Cancel</a>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection