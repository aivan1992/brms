@extends('template.main')

@section('body')

@include('partials.card-header', ['title' => 'Create Account'])


@include('partials.success-error-notification')

<div class="row">
	<div class="col-lg-12 d-flex justify-center ">
		<div class="col-lg-6 grid-margin stretch-card offset-lg-3">
			<div class="card">
				<div class="card-body">
					<form class="forms-sample" action="/accounts" method="POST">
						@csrf
						<div class="form-group">
							<label>Name</label>
							<input type="text" class="form-control" name="name">
						</div>
						<div class="form-group">
							<label>Email address</label>
							<input type="email" class="form-control" name="email">
						</div>
						<div class="form-group">
							<label>Password</label>
							<input type="password" class="form-control" name="password">
						</div>
						<div class="form-group">
							<label>Image upload</label>
							<input type="file" name="img[]" class="file-upload-default">
							<div class="input-group col-xs-12">
								<input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
								<span class="input-group-append">
									<button class="file-upload-browse btn btn-secondary" type="button">Upload</button>
								</span>
							</div>
						</div>
						<div class="form-group">
							<label>User roles</label>
							<select class="form-control" name="role">
								<option value="super-admin">Super-admin</option>
								<option value="admin">Admin</option>
								<option value="customer">Customer</option>
							</select>
						</div>

						<div class="mt-3">
							<button type="submit" class="btn btn-primary mr-2">Submit</button>
							<a class="btn btn-light" href="{{route('accounts.index')}}">Cancel</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection