@extends('template.main')

@section('body')

 @include('partials.card-header', ['title' => 'Edit Product'])

@include('partials.success-error-notification')

<div class="row">
	<div class="col-lg-12 d-flex justify-center ">
		<div class="col-lg-6 grid-margin stretch-card offset-lg-3">
			<div class="card">
				<div class="card-body">

					<form class="forms-sample"  action="{{route('products.update', $product->id)}}" method="POST" enctype="multipart/form-data">
						@method('PUT')
						@csrf
						

						<div class="form-group">
							<label>Product ID</label>
							<input type="text" readonly  value="{{$product->id}}" class="form-control" >
						</div>
						<div class="form-group">
							<label >Product Title</label>
							<input type="text" name="product[title]" class="form-control"  value="{{$product->title}}">
						</div>
						<div class="form-group">
							<label >Product Serial</label>
							<input type="text" class="form-control" name="product[serial]"  value="{{$product->serial}}">
						</div>


						<div class="form-group">
							<label>Status</label>
							<select class="form-control" name="product[status]">
								@if($product->status)
								<option value="1" selected>Available</option>
								<option value="0">Not available</option>
								@else
								<option value="1">Available</option>
								<option value="0"selected>Not available</option>
								@endif
							</select>
						</div>

						 <div class="form-group">
						 	<label>Initial rate</label>
                          <div class="input-group">
                            <div class="input-group-prepend bg-secondary border-primary">
                              <span class="input-group-text bg-transparent text-black">P</span>
                            </div>
                            <input type="text" class="form-control" value="{{$product->initial_rate}}" name="product[initial_rate]" aria-label="Amount (to the nearest dollar)">
                          </div>
                        </div>


						<div class="form-group">
							<label>Succeeding Days Rate</label>
							<div class="input-group">
								<div class="input-group-prepend bg-secondary border-primary">
									<span class="input-group-text bg-transparent text-black">P</span>
								</div>
								<input type="text" class="form-control"  name="product[succeeding_days_rate]"
								value="{{$product->succeeding_days_rate}}"
								 aria-label="Amount (to the nearest dollar)">
							</div>
						</div>


						<div class="form-group">
							<label>Product Category</label>
							<select class="form-control" name="product[category_id]" >
								@foreach($categories as $category)

								@if($product->category()->value('id') == $category->id)
								<option value="{{$category->id}}" selected>{{$category->name}}</option>
								@else
								<option value="{{$category->id}}">{{$category->name}}</option>
								@endif

								@endforeach
							</select>
						</div>
						
						<div class="form-group">
							<label>Image upload</label>
							<div class="input-group">
								<span class="input-group-btn bg-secondary">
									<span class="btn btn-default btn-file">
										Browse… <input type="file" name="image" id="imgInp">
									</span>
								</span>
								<input type="text"  class="form-control bg-transparent"  readonly>
							</div>
							<img id='img-upload'/>
						</div>

						<div class="form-group">
							<label>Notes</label>
							<textarea class="form-control" name="product[description]" rows="2">{{$product->description}}</textarea>
						</div>

						<div class="mt-3">
						<button type="submit" class="btn btn-primary mr-2">Submit</button>
						<a href="{{route('products.index')}}" class="btn btn-light">Cancel</a>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection