  @extends('template.main')

  @section('body')

  @include('partials.card-header', ['title' => 'Product Lists'])

  @include('partials.success-error-notification')
  
  @include('partials.search-form', [ 'route' => '/products/search', 
  'searchKey' => [
  ['value' => 'serial', 'text' => 'SERIAL'],
  ['value' => 'id', 'text' => 'PRODUCT ID'],
  ['value' => 'title', 'text' => 'PRODUCT TITLE']
  ]])


  <div class="col-lg-12 grid-margin stretch-card">
   <div class="card">
    <div class="card-body">
     <table class="table table-hover">
      <thead>
       <tr>
        <th>PRODUCT ID<a href="Javascript:void(0)" title="Click serial to view image"><i class="fa fa-camera text-primary"></i></a></th>
        <th>PRODUCT TITLE</th>
        <th>SERIAL</th>
        <th>CATEGORY</th>
        <th>STATUS</th>
        <th>ACTIONS</th>
        <th>REMOVE PRODUCT</th>
      </tr>
    </thead>
    <tbody>

      @foreach($products as $product)

      <tr>
        <td>
              @include('partials.show-edit-product', ['product_id' => $product->id])
        </td>
        <td>
          <textarea readonly class="form-control">{{$product->title}}</textarea>
        </td>
        <td>
          {{$product->serial}}
        </td>
        <td>
          {{$product->category()->value('name')}}
        </td>
        <td>

         <label class="badge {{$product->status ? 'badge-success' : 'badge-danger'}} custom-min-width-label">{{$product->status ? 'available' : 'unavailable'}}</label>

       <!--<a href="Javascript:void(0)" title="{{$product->status ? 'available' : 'unavailable'}}">
         <i class="fa {{ $product->status ? 'fa-ban text-success' : 'fa-check text-danger'}} "></i></a> -->
       </td>

       <td>
        <ul class="list-group custom-lists-horizontal">
          <li > <a title="edit product" href="{{route('products.edit', $product->id)}}" class="btn social-btn btn-inverse-secondary">
            <i class="fa fa-edit text-black"></i>
          </a>
        </li>
        <li>
          <button type="type" data-toggle="tooltip" title="mark as available" onclick="updateStatus(1, '{{$product->id}}')" class="btn social-btn btn-inverse-secondary">
            <i class="fa fa-check text-black"></i>
          </li>
          <li>
           <a title="mark as unavailable" onclick="updateStatus(0 ,'{{$product->id}}')" class="btn social-btn btn-inverse-secondary">
            <i class="text-black fa fa-ban"></i>
          </a>
        </li>
      </ul>

    </td>
    <td>

      <a onclick="destroyProduct('{{$product->id}}')" title="remove product" class="btn social-btn btn-inverse-secondary">
        <i class="fa fa-trash-o text-black"></i>
      </a>
    </td>
  </tr>

  @endforeach

</tbody>
<tfoot>
  <tr class="custom-pagination-space-around">
    <td><a href="/products/create" class="btn btn-primary btn-fw">Add Product</a></td>
    <td colspan="7">{{$products->links()}}</td> 
  </tr>
</tfoot>
</table>

</div>
</div>
</div>



<script>



  const updateStatus = (value, id) =>{

    if(confirm('Are you sure you want to proceed')) {
     $.ajaxSetup({
      headers: { 'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content }
    });

     $.ajax({
      url: "{{url('/products/status')}}",
      method: 'POST',
      data: { 
       status : value,
       id
     },
     success:  res => {
      alert(JSON.stringify(res.success))
      window.location.reload();
    },  
    catch : err => {
     alert(JSON.stringify(err))
   }})
   }

 }
 const destroyProduct = id => {

  if(confirm('Are you sure you want to proceed')) {
   $.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content }
  })
   $.ajax({
    url: '/products/'+id,
    method: 'DELETE',
    data: { 
     id
   },
   success:  res => {
    alert(JSON.stringify(res.success))
    window.location.reload();
  },  
  catch : err => {
   alert(JSON.stringify(err))
 }})
 }

} 


</script>

@endsection