@extends('template.main')


@section('body')

 @include('partials.card-header', ['title' => 'View Product'])

@include('partials.success-error-notification')

<div class="row">
	<div class="col-lg-12 d-flex justify-center ">
		<div class="col-lg-6 grid-margin stretch-card offset-lg-3">
			<div class="card">
				<div class="card-body">

					

						<div class="form-group">
							<label >Product ID</label>
							<input type="text" readonly  value="{{$product->id}}" class="form-control" >
						</div>
						<div class="form-group">
							<label >Product Title</label>
							<input type="text" name="product[title]" class="form-control"  readonly value="{{$product->title}}">
						</div>
						<div class="form-group">
							<label >Product Serial</label>
							<input type="text" class="form-control" name="product[serial]" readonly  value="{{$product->serial}}">
						</div>


						<div class="form-group">
							<label>Status</label>
							<select class="form-control" name="product[status]" disabled>
								@if($product->status)
								<option value="1" selected>Available</option>
								@else
								<option value="0">Not available</option>
								@endif
							</select>
						</div>

						 <div class="form-group">
						 	<label>Initial rate</label>
                          <div class="input-group">
                            <div class="input-group-prepend bg-secondary border-primary">
                              <span class="input-group-text bg-transparent text-black">P</span>
                            </div>
                            <input type="text" class="form-control" readonly value="{{$product->initial_rate}}" name="product[initial_rate]" aria-label="Amount (to the nearest dollar)">
                          </div>
                        </div>


						<div class="form-group">
							<label>Succeeding Days Rate</label>
							<div class="input-group">
								<div class="input-group-prepend bg-secondary border-primary">
									<span class="input-group-text bg-transparent text-black">P</span>
								</div>
								<input readonly type="text" class="form-control"  name="product[succeeding_days_rate]"
								value="{{$product->succeeding_days_rate}}"
								 aria-label="Amount (to the nearest dollar)">
							</div>
						</div>


						<div class="form-group">
							<label>Product Category</label>
							<select class="form-control" name="product[category_id]" disabled  >
								@foreach($categories as $category)

								@if($product->category()->value('id') == $category->id)
								<option value="{{$category->id}}" selected>{{$category->name}}</option>
								@else
								<option value="{{$category->id}}">{{$category->name}}</option>
								@endif

								@endforeach
							</select>
						</div>
						
						<div class="form-group">
							<label>Image</label>
							
							<img id='img-upload' src="{{$product->image_path}}" />
						</div>


						<div class="form-group">
							<label>Notes</label>
							<textarea readonly class="form-control" name="product[notes]" rows="2">{{$product->notes}}</textarea>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection