	@extends('template.main')

@section('body')

@include('partials.card-header', ['title' => 'Add Product'])

@include('partials.success-error-notification')

<div class="row">
	<div class="col-lg-12 d-flex justify-center ">
		<div class="col-lg-6 grid-margin stretch-card offset-lg-3">
			<div class="card">
				<div class="card-body">

					<form class="forms-sample"  action="{{route('products.store')}}" method="POST" enctype="multipart/form-data">
						@csrf
						<div class="form-group">
							<label >Product title</label>
							<input type="text" name="product[title]" class="form-control" required>
						</div>
						<div class="form-group">
							<label >Product serial</label>
							<input type="text" class="form-control" name="product[serial]">
						</div>


						<div class="form-group">
							<label>Status</label>
							<select class="form-control" name="product[status]">
								<option value="1" >Available</option>
								<option value="0" selected>Not available</option>
							</select>
						</div>

						<div class="form-group">
							<label>Initial rate</label>
							<div class="input-group">
								<div class="input-group-prepend bg-secondary border-primary">
									<span class="input-group-text bg-transparent text-black">P</span>
								</div>
								<input required type="text" class="form-control"  name="product[initial_rate]" aria-label="Amount (to the nearest dollar)">
							</div>
						</div>

						<div class="form-group">
							<label>Succeeding Days Rate</label>
							<div class="input-group">
								<div class="input-group-prepend bg-secondary border-primary">
									<span class="input-group-text bg-transparent text-black">P</span>
								</div>
								<input required type="text" class="form-control"  name="product[succeeding_days_rate]" aria-label="Amount (to the nearest dollar)">
							</div>
						</div>

						<div class="form-group">
							<label>Category</label>
							<select class="form-control" name="product[category_id]">
								@foreach($categories as $category)
								<option value="{{$category->id}}">{{$category->name}}</option>
								@endforeach
							</select>
						</div>



						<div class="form-group">
							<label>Image upload</label>
							<div class="input-group">
								<span class="input-group-btn bg-secondary">
									<span class="btn btn-default btn-file">
										Browse… <input type="file" name="image" required id="imgInp">
									</span>
								</span>
								<input type="text"  class="form-control bg-transparent"  readonly>
							</div>
							<img id='img-upload'/>
						</div>


						<div class="form-group">
							<label>Notes</label>
							<textarea class="form-control" name="product[description]" rows="2"></textarea>
						</div>

						<div class="mt-3">
						<button type="submit" class="btn btn-primary mr-2">Submit</button>
						<a href="{{route('products.index')}}" class="btn btn-light">Cancel</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection