
@extends('template.main')

@section('body')

<div class="row page-title-header">
  <div class="col-lg-12">
    <div class="page-header">
      <h4>DASHBOARD</h4>     

    </div>
  </div>

  <div class="col-md-12">

    <div class="page-header-toolbar">
    </div>
  </div>
</div>
<!-- Page Title Header Ends-->
<div class="row">
  <div class="col-md-12 grid-margin">
    <div class="card">
      <div class="row">
        <div class="col-lg-12 mb-0 mt-4 pb-0">
          <h6 class="w-100 text-center mb-0"><b>JANUARY</b></h6>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-lg-3 col-md-6">
            <div class="d-flex">
              <div class="wrapper">
                <h3 class="mb-0 font-weight-semibold">32,451</h3>
                <h5 class="mb-0 font-weight-medium text-primary">Rental Request</h5>
                <p class="mb-0 text-muted">+0.50%</p>
              </div>
              <div class="wrapper my-auto ml-auto ml-lg-4">
                <canvas height="50" width="100" id="stats-line-graph-1"></canvas>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 mt-md-0 mt-4">
            <div class="d-flex">
              <div class="wrapper">
                <h3 class="mb-0 font-weight-semibold">15,236</h3>
                <h5 class="mb-0 font-weight-medium text-primary">Accepted Request</h5>
                <p class="mb-0 text-muted">+0.54%</p>
              </div>
              <div class="wrapper my-auto ml-auto ml-lg-4">
                <canvas height="50" width="100" id="stats-line-graph-2"></canvas>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 mt-md-0 mt-4">
            <div class="d-flex">
              <div class="wrapper">
                <h3 class="mb-0 font-weight-semibold">7,688</h3>
                <h5 class="mb-0 font-weight-medium text-primary">Cancelled</h5>
                <p class="mb-0 text-muted">+0.76%</p>
              </div>
              <div class="wrapper my-auto ml-auto ml-lg-4">
                <canvas height="50" width="100" id="stats-line-graph-3"></canvas>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 mt-md-0 mt-4">
            <div class="d-flex">
              <div class="wrapper">
                <h3 class="mb-0 font-weight-semibold">1,553</h3>
                <h5 class="mb-0 font-weight-medium text-primary">Completed</h5>
                <p class="mb-0 text-muted">+0.54%</p>
              </div>
              <div class="wrapper my-auto ml-auto ml-lg-4">
                <canvas height="50" width="100" id="stats-line-graph-4"></canvas>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">

  <div class="col-md-12 grid-margin stretch-card">

    <div class="card">
      <div class="card-body">
        <h4 class="card-title mb-0">Market Overview</h4>
        <div class="d-flex align-items-center justify-content-between w-100">
          <p class="mb-0"></p>
          <div class="dropdown">
            <button class="btn btn-outline-secondary" type="button" id="dateSorter" aria-haspopup="true" aria-expanded="false">This year</button>
           
          </div>
        </div>
        <div class="d-flex align-items-end">
         
        </div>
        <canvas class="mt-4" height="100" id="market-overview-chart"></canvas>
      </div>
    </div>

  </div>



  @endsection