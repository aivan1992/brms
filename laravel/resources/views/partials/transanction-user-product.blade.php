<table>
  <tbody>
    <tr>
      <td>TRANSACTION ID:</td>
      <td> {{$transaction_id}}</td>
    </tr>
    <tr>
      <td>CUSTOMER ID:</td>
      <td>
       @include('partials.show-edit-user', ['user_id' => $customer_id])
     </td>
   </tr>
   <tr>
    <td>PRODUCT ID:</td>
    <td>
      @include('partials.show-edit-product', ['product_id' => $product_id])
    </td>
  </tr>
</tbody>
</table>