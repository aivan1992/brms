<nav class="sidebar sidebar-offcanvas pb-5" id="sidebar">


  <ul class="nav navmain">
 <li class="nav-item nav-profile">
      <a href="#" class="nav-link">
        <div class="profile-image">
          <img class="img-xs rounded-circle" src="{{asset('assets/images/faces-clipart/pic-1.png')}}" alt="profile image">
          <div class="dot-indicator bg-success"></div>
        </div>
        <div class="text-wrapper">
          <p class="profile-name">{{ isset(Auth::user()->name) ? Auth::user()->name : ''}}</p>
          <p class="designation">{{   isset(Auth::user()->name) ? Auth::user()->getRoleNames()->first() : ''}}</p>
        </div>
      </a>
    </li>
    <li class="nav-item nav-category">Main Menu</li>

    <li class="nav-item">
      <a class="nav-link" href="/accounts">
        <i class="menu-icon typcn typcn-document-text"></i>
        <span class="menu-title">Accounts</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="/products">
        <i class="menu-icon typcn typcn-document-text"></i>
        <span class="menu-title">Products</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="/categories">
        <i class="menu-icon typcn typcn-document-text"></i>
        <span class="menu-title">Product Categories</span>
      </a>
    </li>

  <!--   <li class="nav-item">
      <a class="nav-link" href="discount">
        <i class="menu-icon typcn typcn-document-text"></i>
        <span class="menu-title">Product Discount</span>
      </a>
    </li>
  -->
  <li class="nav-item nav-category">Rental Request</li>

  <li class="nav-item">
    <a class="nav-link" href="/rental-requests/pending-request">
      <i class="menu-icon typcn typcn-document-text"></i>
      <span class="menu-title">Pending request</span>
    </a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="/rental-requests/accepted-request">
      <i class="menu-icon typcn typcn-document-text"></i>
      <span class="menu-title">Accepted request</span>
    </a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="/rental-requests/customer-cancel-request">
      <i class="menu-icon typcn typcn-document-text"></i>
      <span class="menu-title">Customer Cancel Request</span>
    </a>
  </li>



  <li class="nav-item">
    <a class="nav-link" href="/rental-requests/cancelled-request">
      <i class="menu-icon typcn typcn-document-text"></i>
      <span class="menu-title">Cancelled request</span>
    </a>
  </li>


  <li class="nav-item nav-category">Transactions</li>

  <li class="nav-item">
    <a class="nav-link" href="/transactions/on-going">
      <i class="menu-icon typcn typcn-document-text"></i>
      <span class="menu-title">On going</span>
    </a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="/transactions/completed">
      <i class="menu-icon typcn typcn-document-text"></i>
      <span class="menu-title">Completed</span>
    </a>
  </li>


  <li class="nav-item">
    <a class="nav-link" href="/transactions/void-refunded">
      <i class="menu-icon typcn typcn-document-text"></i>
      <span class="menu-title">Void & Refunded</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="/transactions/remove-record">
      <i class="menu-icon typcn typcn-document-text"></i>
      <span class="menu-title">Remove Records</span>
    </a>
  </li>
</ul>
</nav>

<style>
  .navmain {
position: fixed;
 overflow: hidden;
    max-height: 100vh;
}
  .navmain:hover{
    overflow-y: scroll;
  }
</style>