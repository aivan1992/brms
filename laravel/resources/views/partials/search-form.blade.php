  <form method="POST" id="search" action="{{$route}}">
    @csrf
    <div class="col-lg-12">
     <div class="row">
       <div class="col-lg-4">
        <div class="form-group">
          <div class="input-group">
            <input type="text" class="form-control" name="search" placeholder="Search" aria-label="Search" aria-describedby="colored-addon3" autofocus>
            <div class="input-group-append bg-secondary border-primary">
              <span class="input-group-text bg-transparent">
               <a style="cursor: pointer;" onclick="document.querySelector('#search').submit()">
                <i class="fa fa-search "></i>
              </a>
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
     <div class="form-group">
      <select class="form-control form-control-lg" name="key" required>

        @foreach($searchKey as $key) 
             <option  value="{{$key['value']}}" @if($loop->first) selected @endif>{{$key['text']}}</option>
        @endforeach
  
      </select>
    </div>
  </div>
</div>
</div>
</form>