 <!-- custom errors and success notification -->



 @if(session('success'))
 <div class="col-lg-12">
    <code>{{session('success')}}</code>
</div>
@endif


@if($message = Session::get('error'))
<div class="col-lg-12">
    <div class="code">{{$message}}</code>

</div>
@endif


@if(count($errors) > 0 ) 
<div class="col-lg-12">
    @foreach($errors->all() as $error)
    <code>{{$error}}</code>
    @endforeach
</div>
@endif


