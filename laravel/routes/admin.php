<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/', 'BackEnd\MainController@index');


// Route::group(['middleware' => ['auth', 'role:super-admin, sub-admin']], function () {


Route::group(['namespace' => 'BackEnd'], function () { 

Route::post('/accounts/search', 'AccountsResource@search');
Route::resource('/accounts', 'AccountsResource');

Route::resource('/categories', 'CategoriesResource');

Route::post('/products/search', 'ProductsResource@search');
Route::post('/products/status', 'ProductsResource@updateStatus')->name('products.status');	
Route::resource('/products', 'ProductsResource');

Route::get('/rental-requests/{page?}', 'RentalRequestsResource@index');
Route::post('/rental-requests/status', 'RentalRequestsResource@updateStatus');	
Route::resource('/rental-requests/', 'RentalRequestsResource')->except('index');


Route::post('/transactions/update-start-end', 'TransactionsResource@updateStartEnd');
Route::get('/transactions/{page?}', 'TransactionsResource@index');
Route::post('/transactions/status', 'TransactionsResource@updateStatus');
Route::resource('/transactions/', 'TransactionsResource')->except('index');

});

// });

