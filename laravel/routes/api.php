<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['namespace' => 'Api'], function () {

	Route::post('login', 'AuthController@login');
	Route::post('register', 'AuthController@register');
	Route::get('products/new-arrivals', 'ProductController@newArrivals');
	Route::get('products/categories', 'ProductController@categories');
	Route::resource('products', 'ProductController');
	Route::post('activities/subscribe', 'ActivityController@subscribe');
	
	
	Route::group(['middleware' => 'auth:api'], function () {


		Route::resource('account', 'AccountController');
		
		Route::post('activities/add-to-cart', 'ActivityController@addToCart');
		Route::post('activities/get-cart-count', 'ActivityController@getCartCount');

		Route::post('activities/add-to-favorite', 'ActivityController@addToFavorite');


		Route::post('activities/contact', 'ActivityController@contact');


		Route::post('transactions/submit-request', 
			'TransactionController@submitRequest');
		Route::post('transactions/get-cart-products', 
			'TransactionController@getCartProducts');
		Route::post('transactions/remove-to-cart', 
			'TransactionController@removeToCart');

		Route::get('transactions/accepted', 
			'TransactionController@getAccepted');

		Route::get('transactions/pendings', 
			'TransactionController@getPendings');

		Route::get('transactions/on-going', 
			'TransactionController@getOnGoing');

		Route::get('transactions/completed', 
			'TransactionController@getCompleted');

		Route::get('transactions/favorites', 
			'TransactionController@getFavorites');

		Route::post('transactions/request-rental-cancellation', 
			'TransactionController@reqestRentalCancellation');



	});
});



