<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


Route::get('', function () {
   
});

Route::get('/', 'HomeController@index');


Route::group(['middleware' => ['auth','role:super-admin|sub-admin']], function () {

Route::get('/mail', 'HomeController@sendEmail');

Route::get('/', 'HomeController@index');

Route::group(['namespace' => 'AdminPanel'], function () { 

Route::post('/accounts/search', 'AccountsResource@search');
Route::resource('/accounts', 'AccountsResource');


Route::post('/categories/search', 'CategoriesResource@search');
Route::resource('/categories', 'CategoriesResource');


Route::post('/products/search', 'ProductsResource@search');
Route::post('/products/status', 'ProductsResource@updateStatus')->name('products.status');	
Route::resource('/products', 'ProductsResource');

Route::post('/rental-requests/update-approved-qty', 'RentalRequestsResource@updateApprovedQty');

Route::get('/rental-requests/{page?}', 'RentalRequestsResource@index');
Route::post('/rental-requests/remove-record', 'RentalRequestsResource@removeRecord');
Route::post('/rental-requests/status', 'RentalRequestsResource@updateStatus');	
Route::resource('/rental-requests/', 'RentalRequestsResource')->except('index');
Route::post('/rental-requests/update-start-end', 'RentalRequestsResource@updateStartEnd');

Route::post('/transactions/deny-request', 'TransactionsResource@denyRequest');
Route::post('/transactions/update-start-end', 'TransactionsResource@updateStartEnd');
Route::get('/transactions/{page?}', 'TransactionsResource@index');
Route::resource('/transactions/', 'TransactionsResource')->except('index');

});


});



