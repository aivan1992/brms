<?php

namespace App\Console\Commands;

use App\Jobs\SendRentalReminder;
use App\Mail\UserRentalReminder;
use App\Models\Rental;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\HttpFoundation\Response;

class CheckForExpiringRentals extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CheckForExpiringRentals:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if rental is expiring.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $rentals = Rental::whereStatus('on-going')->whereCancelledAt(null)->whereCompletedAt(null)->whereNotNull('date_end')->get();

        foreach ($rentals as $rental) {
           $dateYearNow = date('M j, Y', strtotime(Carbon::now()));
           $dateOfEnd = date('M j, Y', strtotime($rental->date_end));
           if($dateYearNow == $dateOfEnd) {
             // SendRentalReminder::dispatch(
             //    , 
             //    $dateOfEnd);

              \Mail::to($rental->customer()->value('email'))
        ->send(new UserRentalReminder($rental));

        if(count(\Mail::failures()) > 0 ) {
            return response()->json(['error' => 'error occured!'], Response::HTTP_BAD_REQUEST);
        }


         }            

     }
 }
}
