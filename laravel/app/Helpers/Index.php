<?php

function tJson($value)
{
	$xml = simplexml_load_string($value);
	$json = json_encode($xml);
	$array = json_decode($json, TRUE);
	return $array;
}

function platformSlashes($path)
{
	return str_replace('/', DIRECTORY_SEPARATOR, $path);
}

function globalCalculateQty($old, $new)
{
	$calculation = abs($old - $new);
	if($old > $new) {
		$calculation = '-'.$calculation;
	} elseif ($old < $new) {
		$calculation = '+'.$calculation;
	}
	return $calculation;
}

function globalConvertKeys($arr)
{
	return array_map(function($item){
		if(is_array($item))
			$item = globalConvertKeys($item);
		return $item;
	},array_change_key_case($arr));
}

function toSnakeCase($text) 
{
	return strtolower(preg_replace(['/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/'], '$1_$2', $text));
}

function removeprefixSuffixes($sku, $prefixSuffixes)
{
	foreach ($prefixSuffixes as $value) {
		$sku = str_replace($value,"",$sku);
	}
	return $sku;
}


function toKebabCase($text)
{
	if($text) {
		$text = strtolower(preg_replace('/(?<!^)[A-Z]/', '-$0', $text));
	}
	return $text;
}
