<?php

namespace App;

use App\Models\Product;
use App\Models\Rental;
use App\Models\UserDetail;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
class User extends Authenticatable
{
    use Notifiable, HasRoles, HasApiTokens, AuthenticableTrait, Sluggable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'slug'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function setNameAttribute($value)
    {
        $this->attributes['name'] =  Str::title($value);
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }     

    public function jsPermissions() {

        return json_decode([
           'role' =>$this->getRoleNames()->first(),
           'permission' => $this->getAllPermissions()->pluck('name')
       ]);
    }

    public function getUserRoleAttribute() {

           return $this->getRoleNames()->first();
    }
    
    public function cart()
    {
        return $this->belongsToMany(Product::class, 'carts', 'user_id' , 'product_id');
    } 

      public function userDetails()
    {
        return $this->hasOne(UserDetail::class, 'user_id');
    } 

     public function favorite()
    {
        return $this->belongsToMany(Product::class, 'favorites', 'user_id' , 'product_id');
    } 

       public function rentals()
    {
        return $this->hasMany(Rental::class, 'customer_id');
    } 

         public function customerCancelRequest()
    {
        return $this->belongsToMany(Rental::class, 'customer_cancel_request', 'user_id' , 'rental_id');
    } 

    
}
