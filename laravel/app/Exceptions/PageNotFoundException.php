<?php

namespace App\Exceptions;

use Exception;

class PageNotFoundException extends Exception
{
    public function render()
	{ 
		return response()->view('back-end.pages.404');

	}
}
