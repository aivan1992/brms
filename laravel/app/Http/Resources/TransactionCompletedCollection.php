<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class TransactionCompletedCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
         return [
            'product_slug' => $this->product()->value('slug'),
            'image' =>  url('/') . $this->product->imagePath,
            'product_id' => $this->product_id,
            'product_title' => $this->product()->value('title'),
            'start_end' => $this->rentalStartEnd,
            'penalty' => $this->penaltyAmountMutated == null ? 0 : $this->penaltyAmountMutated,
            'amount' => $this->totalRentAmount,
            'transaction_id' =>  $this->transaction_id,
            'completed_at' => $this->completedAtMutated,
        'approved_qty' => $this->approved_qty,
            
        ];
    }
}
