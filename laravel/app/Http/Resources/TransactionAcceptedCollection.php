<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class TransactionAcceptedCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       return [
        'product_slug' => $this->product()->value('slug'),
        'image' =>  url('/') . $this->product->imagePath,
        'transaction_id' =>  $this->transaction_id,
        'product_id' => $this->product_id,
        'approved_at' => $this->approvedAtMutated,

        'amount' => $this->totalRentAmountMutated,
        'product_title' => $this->product()->value('title'),
    ];
}
}
