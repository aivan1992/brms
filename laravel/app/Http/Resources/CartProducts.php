<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CartProducts extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $initial_rate = number_format($this->initial_rate, 2);
        $succeeding_days_rate = number_format($this->succeeding_days_rate, 2);

        return [
         'image' => url('/') . $this->imagePath,   
         'initial_rate' => $initial_rate,
         'succeeding_days_rate' => $succeeding_days_rate,
         'status' => $this->status,
         'title' => $this->title,
         'slug' => $this->slug,
         'requested_days' => 1,
         'qty' => 1,
         'total' => 0,
         'product_id' => $this->id
     ];
 }
}
