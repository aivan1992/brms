<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Request as Req;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {


        $initial_rate = number_format($this->initial_rate, 2);
        $succeeding_days_rate = number_format($this->succeeding_days_rate, 2);
        
         return [

            'id' => $this->id,
            'slug' => $this->slug,
            'category' => $this->category()->value('name'),
            'initial_rate' => $initial_rate,
            'status' => $this->status == 1 ? 'available' : 'not available',

            'title' => $this->title,
            'slug' => $this->slug,
            'description' => $this->description,

            'serial' => $this->serial,

            'succeeding_days_rate' => $succeeding_days_rate,

            'image' =>  url('/') . $this->imagePath,

            'price' => $initial_rate . '-' . $succeeding_days_rate . 
            '/' . 'day', 
        ];
    }
}
