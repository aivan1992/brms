<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class TransactionPendingCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $rental_requested = $this->customerCancelRequest->first() ? 1 : 0;

     return [
        'product_slug' => $this->product()->value('slug'),
        'image' =>  url('/') . $this->product->imagePath,
        'transaction_id' =>  $this->transaction_id,
        'product_id' => $this->product_id,
         'rental_id' => $this->id,
        'approved_at' => $this->approvedAtMutated,
        'product_title' => $this->product()->value('title'),
        'requested_at' => $this->createdAtMutated,
        'amount' => $this->totalRentAmountMutated,

        'rental_requested' => $rental_requested
    ];

}
}
