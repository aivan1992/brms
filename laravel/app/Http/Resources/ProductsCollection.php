<?php

namespace App\Http\Resources;

use App\Models\Favorite;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Auth;
use Request as Req;

class ProductsCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        // ['title', 'serial', 'description', 'slug','initial_rate', 'status','category_id', 'succeeding_days_rate'];

        
          if(Req::has('Authorization') || Req::header('Authorization')) {
            $user = Auth::guard('api')->user();

            if(Favorite::whereUserId($user->id)->whereProductId($this->id)->first()) {
               $favorite = 1;    
            } else {
                $favorite = 0;
            }
        }


        $initial_rate = number_format($this->initial_rate, 2);
        $succeeding_days_rate = number_format($this->succeeding_days_rate, 2);

        return [

            'id' => $this->id,
            'slug' => $this->slug,
            'category' => $this->category()->value('name'),
            'initial_rate' => $initial_rate,
            'status' => $this->status == 1 ? 'available' : 'not available',

            'title' => $this->title,
            'slug' => $this->slug,
            'description' => $this->description,

            'serial' => $this->serial,

            'succeeding_days_rate' => $succeeding_days_rate,

            'image' =>  url('/') . $this->imagePath,

            'price' => 'P' . $initial_rate . '/' . 'Day', 

            'favorite' => $favorite ?? 0
        ];
    }
}
