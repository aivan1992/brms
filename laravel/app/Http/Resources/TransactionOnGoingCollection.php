<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class TransactionOnGoingCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'product_slug' => $this->product()->value('slug'),
            'image' =>  url('/') . $this->product->imagePath,
            'product_id' => $this->product_id,
            'product_title' => $this->product()->value('title'),
            'start_end' => $this->rentalStartEnd,
             'penalty' => $this->penalty_amount == null ? 0 : $this->penalty_amount,
            'transaction_id' =>  $this->transaction_id,
              'amount' => $this->totalRentAmountMutated,
        'approved_qty' => $this->approved_qty,

            'initial_amount' => 'awdawd'
        ];
    }
}
