<?php

namespace App\Http\Controllers\AdminPanel;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Spatie\Permission\Models\Role;

class AccountsResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $model; 

    public function  __construct() {

      $this->model = new User;    

    }  


    public function index()
    {

     $accounts =  $this->model::paginate(10);
     return view('pages.accounts.index')
     ->with('accounts',$accounts);
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('pages.accounts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

     DB::beginTransaction();

     try {

       $request->validate([
        'name' => "required|string|max:255",
        'email' => "required|string|email|max:255|unique:users",
      ]);
       
       

       $user =  $this->model::create([
         'name' => $request->name,
         'email' => $request->email,
         'password' => Hash::make($request->password),  
       ]);

        /*
        *  CREATE ROLE IF NOT EXIST
        */ 
        if(!Role::whereName($request->role)->exists()) {

         $role = Role::create(['name' => $request->role]);

       }

       /*
       *  ASSIGN ROLE
       */
       $user->assignRole($request->role);

       DB::commit();

       return Redirect::back()->with('success','Data  Saved  Successfully!');

     } catch (\Exception $e) {

       DB::rollback();

       return Redirect::back()->with('error', $e->getMessage());

     }

   }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

      $account =  $this->model::find($id);

      return view('pages.accounts.show')
      ->with('account', $account);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

     $account =  $this->model::find($id);

     return view('pages.accounts.edit')
     ->with('account', $account);
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

     $user =  $this->model::find($id);

     $request->validate([
      'name' => "required|string|max:255",
      'email' => "required|string|email|max:255",
    ]);

     /*
      * DECLARE ARRAY VARIABLE
      */
     $details_array = array(
      'name' => $request->name,
      'email' => $request->email,
    );
        /*
        *
        */

      /*
      * CHECK IF PASSWORD HAS INPUT
      */
      if($request->password != '') {

       $request->validate([
         'password' => "string|min:8",
       ]);   

       $password = array('password' => Hash::make($request->password));

       // PUSH PASSWORD INPUT TO ARRAY
       $merged_array = array_merge($details_array, $password);

     }
      /*
      * 
      */

      DB::beginTransaction();

      try {

      /*
      * UPDATE RECORDS
      */
      $user->update($merged_array ?? $details_array);
      /*
      * 
      */

        /*
        *  CREATE ROLE IF NOT EXIST
        */ 
        if(!Role::whereName($request->role)->exists()) {

         $role = Role::create(['name' => $request->role]);

       }
       /*
       *
       */

       /*
       *  UPDATE ROLE
       */
       $user->syncRoles($request->role);
        /*
       *
       */

        DB::commit();

        return Redirect::back()->with('success','Data  Updated  Successfully!');

      } catch (\Exception $e) {

       DB::rollback();

       return Redirect::back()->with('error', $e->getMessage());

     }

   }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

      $user =  $this->model::find($id);

      DB::beginTransaction();

      try {

       $user->destroy($id);

       DB::commit();

       return response()->json(['success'=>'User Suspend Success!'], 200);   

     } catch (\Exception $e) {

       DB::rollback();

       return response()->json(['error' => $e->getMessage()], 500);

     }
   }

   public function search(Request $request) {


     $search = $request->search;

     if($request->search != null && $request->key != null) {

         /*
         *  SEARCH FOR EXISTING DATA
         */
         $accounts = $this->model::where($request->key, 'LIKE', '%' . $search . '%')
        // ->orWhere('email', 'LIKE', '%' . $search . '%')
         ->paginate(10)->setPath('/');
        /*
         *  
         */

        if (count($accounts) > 0) {

          switch (count($accounts)) {

            case 1:
            $success_message = '1 record has been found';
            break;
            
            default:
            $success_message = count($accounts) . ' records has been found';
            break;

          }


        /*
         * DECLARE SUCCESS SESSION MESSAGE AND REMOVE ERROR SESSION
         */
        $request->session()->forget('error');
        $request->session()->flash('success', $success_message);
        /*
         *
         */

        return view('pages.accounts.index')
        ->with('accounts', $accounts);

      }

    }

        /*
         * DECLARE ERROR SESSION MESSAGE AND REMOVE SUCCESS SESSION
         */ 
        $request->session()->forget('success');
        $request->session()->flash('error', 'No specific record found!');
        /*
         *
         */

        return view('pages.accounts.index')
        ->with('accounts',  $this->model::paginate(10));

      }
      
    }
