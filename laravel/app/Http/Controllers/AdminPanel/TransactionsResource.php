<?php

namespace App\Http\Controllers\AdminPanel;

use App\Http\Controllers\Controller;
use App\Models\Rental;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TransactionsResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($page = null)
    {

        $prefix = 'pages/transactions/';

        switch($page) {

            case "on-going": 
            $rentals = Rental::whereStatus('on-going')->paginate(10);
            return view($prefix . $page)
            ->with('rentals', $rentals);
            return view($prefix . $page);
            break;

            case "completed": 
            $rentals = Rental::whereStatus('completed')->paginate(10);
            return view($prefix . $page)
            ->with('rentals', $rentals);
            break;

            case "void-refunded": 
            $rentals = Rental::whereStatus('void-refunded')->paginate(10);
            return view($prefix . $page)
            ->with('rentals', $rentals);
            break;

            case "remove-record": 
            $rentals = Rental::paginate(10);
            return view($prefix . $page)
            ->with('rentals', $rentals);
            break;

            default: 
            return redirect('/');

        } 
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
  /**
    *
    *  UPDATE THE COLUMN DATE_START & DATE_END   
    *
    */
  public function updateStartEnd(Request $request) {

      $rental = Rental::find($request['id']);

      $rental->update([
        'date_start' => $request['date_start'],
        'date_end' => $request['date_end']
    ]);

      return response()->json(['success'=>'The Record has been updated.'], 200);

  }

    // public function customerRentalCancellation(Request $request) {

    //    $user = User::find($request->user()->id);

    //    $rental = Rental::find($request->rental_id);

    //    $user->customerCancellation()->

    // }


  public function denyRequest(Request $request) {
     $user = User::find($request->user_id);
     $rental = Rental::find($request->rental_id);
     $user->customerCancelRequest()->detach($rental);
     return response()->json(['success'=>'The request has been denied'], 200);
 }

}
