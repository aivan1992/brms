<?php

namespace App\Http\Controllers\AdminPanel;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CategoriesResource extends Controller
{


   private $model; 

   public function  __construct() {

      // $categories = Category::get(['name', 'id']);

      // View::share('categories', $categories);   

      $this->model = new Category;  

  } 


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $model =  $this->model::paginate(10);

        return view('pages.categories.index')
        ->with('categories', $model);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('pages.categories.create');
  }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $request->validate([
            'name' => "required|string|max:255|unique:categories",
        ]);

        $this->model::create($request->all());

        return Redirect::back()->with('success','Data Saved Successfully!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model =  $this->model::find($id);

        return view('pages.categories.show')
        ->with('model', $model);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

         $model =  $this->model::find($id);

        return view('pages.categories.edit')
        ->with('model', $model);

 }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
     $model = $this->model::findOrFail($id);

     $model->update(
        $request['category']);

     return Redirect::back()->with('success','Data Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::destroy($id);

        return response()->json(['success'=>'The DELETE has been updated.'], 200);
    }

     public function search(Request $request) {

         $search = $request->search;

         if($request->search != null) {

         /*
         *  SEARCH FOR EXISTING DATA
         */
         $categories = $this->model::where('name', 'LIKE', '%' . $search . '%')
        // ->orWhere('email', 'LIKE', '%' . $search . '%')
         ->paginate(10);
        /*
         *  
         */

        if (count($categories) > 0) {

          switch (count($categories)) {

            case 1:
            $success_message = '1 record has been found';
            break;
            
            default:
            $success_message = count($categories) . ' records has been found';
            break;

        }


        /*
         * DECLARE SUCCESS SESSION MESSAGE AND REMOVE ERROR SESSION
         */
        $request->session()->forget('error');
        $request->session()->flash('success', $success_message);
        /*
         *
         */

        return view('pages.categories.index')
        ->with('categories', $categories);

    }

}

        /*
         * DECLARE ERROR SESSION MESSAGE AND REMOVE SUCCESS SESSION
         */ 
        $request->session()->forget('success');
        $request->session()->flash('error', 'No specific record found!');
        /*
         *
         */

        return view('pages.categories.index')
        ->with('categories', $this->model::paginate(10));

    }




}
