<?php

namespace App\Http\Controllers\AdminPanel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductsCollection;
use App\Models\Category;
use App\Models\Image;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class ProductsResource extends Controller
{

 private $model; 

 public function  __construct() {
  $categories = Category::get(['name', 'id']);
  View::share('categories', $categories);   
  $this->model = new Product;  
} 

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $products  = ProductsCollection::collection(Product::all());  
        $products =  $this->model::paginate(10);
        return view('pages.products.index')
        ->with('products', $products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        // SAVE DATE TO GENERATE SLUG
        $product =  $this->model::create($request['product']);
        /*
        *  PREPARE IMAGE
        * 
        */
        $directory = 'files/products/'. $product->id;
        $image = $request->file('image');
        $ext = $image->getClientOriginalExtension();
        // PARSE PRODUCT SLUG AND IMG EXTENSION 
        $file_name =  $product->slug . "." . $ext;
        $imageInfo = new Image([
            'file_name' => $file_name,
            'directory_name' => $directory
        ]);
        /*
        */
        // SAVE IMAGE
        $product->images()->save($imageInfo);
        // MOVE IMAGE
        $request->image->move($directory, $file_name);
        return Redirect::back()->withInput($request->all())->with('success','Data Saved Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $product =  $this->model::find($id);
       return view('pages.products.show')
       ->with('product', $product);
   }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product =  $this->model::find($id);
        return view('pages.products.edit')
        ->with('product', $product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        // $product = new Rroduct()
     $product = Product::findOrFail($id);
     if($request->hasFile('image')) {
       $request->validate([
          'image' => 'required|image|mimes:jpeg,png,jpg',
      ]);
       $path = 'files/products/' . $product->id;
          /*
          * SEARCH FOR PO FILES
          */
          if(\File::exists($path)){
           \File::deleteDirectory($path);
       }
       $product->images()->delete();

       $image = $request->file('image');
       $ext = $image->getClientOriginalExtension();
        // PARSE PRODUCT SLUG AND IMG EXTENSION 
       $file_name =  $product->slug . "." . $ext;
       $imageInfo = new Image([
        'file_name' => $file_name,
        'directory_name' => $path
    ]);
        /*
        */
        // SAVE IMAGE
        $product->images()->save($imageInfo);
        $request->image->move($path, $file_name);
    }
    $product->update(
        $request['product']
    );
    return Redirect::back()->with('success','Data Updated Successfully!');
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $product = $this->model::find($id);
       $path = 'files/products/'. $product->slug;
          /*
          * SEARCH FOR PO FILES
          */
          if(\File::exists($path)) {
             \File::deleteDirectory($path);
         }
         $product->images()->delete();
         $product->delete();
         return response()->json(['success'=>'The DELETE has been updated.'], 200);
     }

     public function search(Request $request) {
         $search = $request->search;
         if($request->search != null && $request->key != null) {
         /*
         *  SEARCH FOR EXISTING DATA
         */
         $products = $this->model::where($request->key, 'LIKE', '%' . $search . '%')
        // ->orWhere('email', 'LIKE', '%' . $search . '%')
         ->paginate(10)->setPath('/');
        /*
         *  
         */
        if (count($products) > 0) {
          switch (count($products)) {
            case 1:
            $success_message = '1 record has been found';
            break;
            default:
            $success_message = count($products) . ' records has been found';
            break;
        }
        /*
         * DECLARE SUCCESS SESSION MESSAGE AND REMOVE ERROR SESSION
         */
        $request->session()->forget('error');
        $request->session()->flash('success', $success_message);
        /*
         *
         */
        return view('pages.products.index')
        ->with('products', $products);
    }
}
        /*
         * DECLARE ERROR SESSION MESSAGE AND REMOVE SUCCESS SESSION
         */ 
        $request->session()->forget('success');
        $request->session()->flash('error', 'No specific record found!');
        /*
         *
         */
        return view('pages.products.index')
        ->with('products', $this->model::paginate(10));
    }
    public function updateStatus(Request $request) {
        $product =  $this->model::find($request->id);
        $product->status = $request->status;
        $product->save();
        return response()->json(['success'=>'The Record has been updated.'], 200);
    }





}
