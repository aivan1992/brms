<?php

namespace App\Http\Controllers\AdminPanel;

use App\Http\Controllers\Controller;
use App\Mail\RentalStarted;
use App\Models\Rental;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as Url;
use Symfony\Component\HttpFoundation\Response;

class RentalRequestsResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index($page = null)
     {
        $prefix = 'pages/rental-requests/';
        switch($page) {
            case "pending-request": 
            $rentals = Rental::whereStatus('pending')->orderBy('created_at', 'desc')->paginate(10);
            return view($prefix . $page)
            ->with('rentals', $rentals);
            break;

            case "accepted-request": 
            $rentals = Rental::whereStatus('accepted')->paginate(10);
            return view($prefix . $page)
            ->with('rentals', $rentals);
            break;

            case "customer-cancel-request": 
            $rentals = Rental::whereIn('status', ['pending', 'accepted'])
            ->whereHas('customerCancelRequest')->paginate(10);
            return view($prefix . $page)
            ->with('rentals', $rentals);
            break;

            case "cancelled-request": 
            $rentals = Rental::whereStatus('cancelled')->paginate(10);
            return view($prefix . $page)
            ->with('rentals', $rentals);
            break;

            default: 
            return redirect('/');
        } 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {

     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show($id)
     {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
        return response()->json(['success'=>'The Record has been updated.'], 200);
        //
    }
    
    public function updateStatus(Request $request)
    {
        // return response()->json(['success'=> $request->all()], 200);

        $rental = Rental::findOrFail($request->id);
        $rental->status = $request->status;
        
        if($request->status == 'void-refunded') {

            $rental->void_refunded_at = Carbon::now();

        } else if($request->status == 'cancelled') {

            $rental->cancelled_at = Carbon::now();

        } else if($request->status == 'accepted') {

            $rental->approved_at = Carbon::now();
            $rental->product->update(['status' => 0]);

        }  else if($request->status == 'completed') {

            $rental->completed_at = Carbon::now();
            $rental->product->update(['status' => 1]);

        } else if($request->status == 'on-going') {


            $to =  new \DateTime($rental->date_end);
            $from =  new \DateTime($rental->date_start);
            $intevl = $from->diff($to);
            $totaldays = $intevl->format('%a');


            $initialRate = $rental->product()->value('initial_rate');
            $succRate = $rental->product()->value('succeeding_days_rate');

            if($totaldays > 1) {
               $total = ($totaldays - 1) * $succRate;   
               $total = $initialRate + $total;
           } else {
            $total = $initialRate;
        }

        $total = $total * ($rental->approved_qty ?? 1);

        $rental->update(['total_rent_amount' => $total]);

            // \Mail::to($rental->customer()->value('email'))->send(new RentalStarted($rental));
        }

        $rental->save();

        return response()->json(['success'=>'The Record has been updated.'], 200);
    }


    public function updateStartEnd(Request $request) 
    {

      $rental = Rental::find($request['id']);

      $rental->update([
        'date_start' => $request['date_start'],
        'date_end' => $request['date_end']
        ]);

      return response()->json(['success'=>'The Record has been updated.'], 200);
  }

  public function updateApprovedQty(Request $request) 
  {
      $rental = Rental::find($request['id']);
      $rental->update([
        'approved_qty' => $request['qty']
        ]);
      return response()->json(['success'=>'The Record has been updated.'], 200);
  }

  public function removeRecord(Request $request) 
  {
     $model = Rental::find($request->id); 
     $model->customerCancelRequest()->detach();
     $model->delete();
     return response()->json(['success'=>'The Record has been removed.'], 200);
 }
}
