<?php

namespace App\Http\Controllers;

use App\Exceptions\PageNotFoundException;
use App\Mail\RentalStarted;
use App\Mail\UserRentalReminder;
use App\Models\Rental;
use App\Notifications\RentalReminder;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       // $rentals = Rental::whereStatus('pending')->orderBy('created_at', 'desc')->paginate(10);
       // return view('pages.rental-requests.pending-request')
       // ->with('rentals', $rentals);
       
    $rentals = Rental::whereStatus('pending')->orderBy('created_at', 'desc')->paginate(10);
        return view('pages/rental-requests/pending-request')
        ->with('rentals', $rentals);
      
         $rentals = Rental::where('status', 'on-going')
       ->whereNotNull('date_end')
       ->whereNotNull('date_start')
       ->get();

       foreach ($rentals as $rental) {
        $dateEndPartial =  date('m-d-Y', strtotime($rental->date_end));
        $dateNowPartial = Carbon::now()->format('m-d-Y');

        $dateEnd = strtotime($dateEndPartial);
        $dateNow = strtotime($dateNowPartial);

        if($dateNow > $dateEnd) {
            $penalty = $rental->product()->value('succeeding_days_rate') ?? 0;

            $res = ceil(abs($dateNow - $dateEnd) / 86400);
            $multiplied = ($res * $penalty);

            $rent = Rental::find($rental->id);
            $rent->penalty_days = $res;
            $rent->penalty_amount = $multiplied; 
            $rent->save();

        }

    }

    return 'true';

   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function page($page) 
    {


        // if($this->checkRoute('pages/' . $page)) {
        if($page == 'home') {

            return view('home');
        }

        try {

            return view('pages/' . $page);

        } catch (\Exception $e) {

            throw new PageNotFoundException();

        }

    }


    public function sendEmail(Request $request) {

        try {


        $rental = Rental::find(2);

         \Mail::to('ivanrobert1992@gmail.com')->send(new RentalStarted($rental));

           if( count(\Mail::failures()) > 0 ) {
            abort(500, 'Error occured');
           }

           return response()->json(true, Response::HTTP_OK);

            } catch (\Exception $e) {
            
      return response()->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);

        }


    }

}
