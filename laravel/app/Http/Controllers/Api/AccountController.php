<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\AccountResource;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request as Api;
use Spatie\Permission\Models\Role;
use Symfony\Component\HttpFoundation\Response;

use App\User;


class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

      $model = User::find(\Auth::user()->id);

      $response = new AccountResource($model);

      return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

     $user =  User::find($request->user()->id);

     $request->validate([
      'first_name' => "required|string|max:255",
      'last_name' => "required|string|max:255",
      'email' => "required|string|email|max:255",
    ]);

     /*
      * DECLARE ARRAY VARIABLE
      */

     $name = $request->first_name .' '. $request->last_name; 

     $details_array = array(
      'name' => $name,
      'email' => $request->email
    );
        /*
        *
        */

      /*
      * CHECK IF PASSWORD HAS INPUT
      */
      if($request->password != '') {

       $request->validate([
         'password' => "string|min:8",
       ]);   

       $password = array('password' => Hash::make($request->password));

       // PUSH PASSWORD INPUT TO ARRAY
       $merged_array = array_merge($details_array, $password);

     }
      /*
      * 
      */

      $user->userDetails()->updateOrCreate([], $request->all());

      DB::beginTransaction();

      try {

      /*
      * UPDATE RECORDS
      */
      $user->update($merged_array ?? $details_array);
      /*
      * 
      */

        /*
        *  CREATE ROLE IF NOT EXIST
        */ 
        if(!Role::whereName('customer')->exists()) {

         $role = Role::create(['name' => 'customer']);

       }
       /*
       *
       */

       /*
       *  UPDATE ROLE
       */
       $user->syncRoles('customer');
        /*
       *
       */

       /*
       *  UPDATE PERMISSIONS
       */
       // if($request->role == 'super-admin') {

       //   $user->givePermissionTo(['view', 'create', 'edit' , 'remove']);          

       // } else {

       //   foreach ((Array)$request->permissions as $key => $value) {

       //    $value ? $user->givePermissionTo($key) : $user->revokePermissionTo($key);

       //  }

       $user->givePermissionTo(['view']);
     
      /*
       *
       */

      DB::commit();
      return response()->json(['success'=>'Data updated successfully'], Response::HTTP_OK); 
    } catch (Exception $e) {
      DB::rollback();
      return response()->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
    }

  }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
  }
