<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\CustomerContactInquiry;
use App\Models\Cart;
use App\Models\Favorite;
use App\Models\Product;
use App\Models\Subscription;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;


class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addToCart(Request $request) 
    {
        try {
            if(!Cart::
              whereProductId($request->product_id)
              ->whereUserId($request->user()->id)->first()) 
            {
                $user = User::find($request->user()->id);
                $product = Product::find($request->product_id);
                $user->cart()->attach($product);

                return response()->json(true, Response::HTTP_OK);
            } else {
                return response()->json(false, Response::HTTP_OK);
            }
        } catch(\Exception $e) {
           return response(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
       }
   }

   public function getCartCount(Request $request) 
   {
    try {
        $user = User::find($request->user()->id);
        $count = $user->cart()->count();
        return response()->json(['count' => $count], Response::HTTP_OK);
    } catch(\Exception $e) {
       return response(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
   }
}

public function addToFavorite(Request $request) 
{
    try {
        $user = User::find($request->user()->id);
        $product = Product::find($request->product_id);
        if(!Favorite::
          whereProductId($request->product_id)
          ->whereUserId($request->user()->id)->first()) 
        {
            $user->favorite()->attach($product);
            return response()->json(true, Response::HTTP_OK);
        } else {
           $user->favorite()->detach($product);
           return response()->json(false, Response::HTTP_OK);
       }
   } catch(\Exception $e) {
       return response(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
   }
}

public function subscribe(Request $request)
{
    if(Subscription::whereEmail($request->email)->first()) {
        return response()->json(false, Response::HTTP_BAD_REQUEST);
    } else {
        Subscription::create([
            'email'=> $request->email,
            'created_at' => Carbon::now() 
        ]);
        return response()->json(true, Response::HTTP_OK);
    }
}


public function contact(Request $request)
{
    $info = $request->all();   
    \Mail::to('annamariecaraulia@gmail.com')->send(new CustomerContactInquiry($info));
    return response()->json(true, Response::HTTP_OK);

}  


}
