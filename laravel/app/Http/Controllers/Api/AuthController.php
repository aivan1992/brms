<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use cors;

class AuthController extends Controller
{

  public function login(Request $request) {
    try {
      $validator = Validator::make($request->all(), [ 
        'email' => 'required|email', 
        'password' => 'required', 
      ]);
      if ($validator->fails()) { 
        abort(Response::HTTP_UNAUTHORIZED, 'Validation fails, please check inputs');   
      }   
      if(!User::whereEmail(request('email'))->first()){
        abort(Response::HTTP_UNAUTHORIZED, 'Email not exist, please register.');
      }
      if(Auth::attempt(['email' => request('email'), 'password' => request('password')])) { 
        $user = Auth::user();
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['name'] =  $user->name;
        $success['role'] =  $user->getRoleNames()->first();
        $success['email'] =  $user->email;
        $success['user_id'] =  $user->id;
        $success['permissions'] = $user->getAllPermissions()->pluck('name'); 
        return response()->json(['success' => $success], Response::HTTP_OK);
      } 
      else{ 
        abort(Response::HTTP_UNAUTHORIZED, 'Password incorrect, please try again.');
      } 
    } catch (\Exception $e) {
      return response(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
    }
  }
    /** 
     * REGISTER API 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 
      $validator = Validator::make($request->all(), [ 
        'name' => 'required', 
        'email' => 'required|email|unique:users', 
        'password' => 'required', 'string', 'min:8',
      ]);
      if ($validator->fails()) { 
        return response()->json(['error'=>$validator->errors()],  Response::HTTP_UNAUTHORIZED);    
      }
      DB::beginTransaction();
      try {
        $input = $request->all(); 
        if(!Role::whereName('customer')->exists()) {
         $role = Role::create(['name' => 'customer']);
       }
       $input['password'] = bcrypt($input['password']); 
       $user = User::create($input); 
       /*
       *  ASSIGN ROLE
       */
       $user->assignRole('customer');
       // foreach ((Array)$request->permissions as $permission) {
       //   $user->givePermissionTo($permission);
       // }
       $user->givePermissionTo(['view']);
       $success['token'] =  $user->createToken('MyApp')->accessToken; 
       $success['name'] =  $user->name;
       $success['role'] =  $user->getRoleNames()->first();
       $success['email'] =  $user->email;
       $success['permissions'] = $user->getAllPermissions()->pluck('name');
       DB::commit(); 
       return response()->json(['success'=> $success], Response::HTTP_CREATED); 
     } catch (Exception $e) {
      DB::rollback();
      return response()->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
    }
  }

}
