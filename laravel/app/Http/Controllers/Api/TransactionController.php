<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CartProducts;
use App\Http\Resources\ProductsCollection;
use App\Http\Resources\TransactionAcceptedCollection;
use App\Http\Resources\TransactionCompletedCollection;
use App\Http\Resources\TransactionOnGoingCollection;
use App\Http\Resources\TransactionPendingCollection;
use App\Models\Product;
use App\Models\Rental;
use App\Models\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TransactionController extends Controller
{

	public function submitRequest(Request $request) {
		$user_id = $request->user()->id;
		foreach ($request->products as $value) {
			$transaction = Transaction::create([
				'created_at', Carbon::now(),
				'user_id' => $user_id
			]);
			Rental::create([
				'customer_id' => $user_id,
				'product_id' => $value['product_id'],
				'transaction_id' => $transaction->id,
				'requested_days' => $value['requested_days'],
				'status' => 'pending',
				'qty' => $value['qty'],
				'approved_qty' => $value['qty'],
				'payment_status' => 0,
				'total_rent_amount' => $value['total'],
				'created_at' => Carbon::now()
			]);
		}
		User::find($user_id)->cart()->detach();
		return response()->json(true, Response::HTTP_OK);
	}

	public function getCartProducts(Request $request) {
		try {
			$user = User::find($request->user()->id);
			$productsCollection = CartProducts::collection($user->cart);
			return response()->json(['products' =>  $productsCollection], Response::HTTP_OK);
		} catch(Exception $e) {
			return response()->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
		}
	}

	public function removeToCart(Request $request) {
		try {
			if($request->product_id) {
				$user = User::find($request->user()->id);
				$product = Product::find($request->product_id);
				$user->cart()->detach($product);
				return response()->json(true, Response::HTTP_OK);	
			}
			abort(500, 'product id is empty');
		} catch(Exception $e) {
			return response()->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
		}
	}

	public function getAccepted(Request $request) {
		$model = Rental::whereCustomerId($request->user()->id)
		->whereStatus('accepted')
		->paginate(10);
		$modelResource = TransactionAcceptedCollection::collection($model);
		$response = [
			'pagination' => [
				'total' => $model->total(),
				'per_page' => $model->perPage(),
				'current_page' =>  $model->currentPage(),
				'last_page' =>  $model->lastPage(),
			],
			'data' =>  $modelResource
		];
		return response()->json($response, Response::HTTP_OK);
	}

	public function getPendings(Request $request) {
		$model = Rental::whereCustomerId($request->user()->id)
		->whereStatus('pending')
		->paginate(10);
		$modelResource = TransactionPendingCollection::collection($model);
		$response = [
			'pagination' => [
				'total' => $model->total(),
				'per_page' => $model->perPage(),
				'current_page' =>  $model->currentPage(),
				'last_page' =>  $model->lastPage(),
			],
			'data' =>  $modelResource
		];
		return response()->json($response, Response::HTTP_OK);
	}

	public function getOnGoing(Request $request) {
		$model = Rental::whereCustomerId($request->user()->id)
		->whereStatus('on-going')
		->paginate(10);
		$modelResource = TransactionOnGoingCollection::collection($model);
		$response = [
			'pagination' => [
				'total' => $model->total(),
				'per_page' => $model->perPage(),
				'current_page' =>  $model->currentPage(),
				'last_page' =>  $model->lastPage(),
			],
			'data' =>  $modelResource
		];
		return response()->json($response, Response::HTTP_OK);
	}

	public function getCompleted(Request $request) {
		$model = Rental::whereCustomerId($request->user()->id)
		->whereStatus('completed')
		->paginate(10);
		$modelResource = TransactionCompletedCollection::collection($model);
		$response = [
			'pagination' => [
				'total' => $model->total(),
				'per_page' => $model->perPage(),
				'current_page' =>  $model->currentPage(),
				'last_page' =>  $model->lastPage(),
			],
			'data' =>  $modelResource
		];
		return response()->json($response, Response::HTTP_OK);	
	}



	public function getFavorites(Request $request) {
		$model = User::find($request->user()->id)->favorite()->paginate(10);
		$modelResource = ProductsCollection::collection($model);
		$response = [
			'pagination' => [
				'total' => $model->total(),
				'per_page' => $model->perPage(),
				'current_page' =>  $model->currentPage(),
				'last_page' =>  $model->lastPage(),
			],
			'data' =>  $modelResource
		];
		return response()->json($response, Response::HTTP_OK);	
	}

	  // public function customerRentalCancellation(Request $request) {
   //  }

	public function reqestRentalCancellation(Request $request) {
		$user = User::find($request->user()->id);
		$rental = Rental::find($request->rental_id);
		$user->customerCancelRequest()->attach($rental);
		return response()->json(true, Response::HTTP_OK);
	}

}
