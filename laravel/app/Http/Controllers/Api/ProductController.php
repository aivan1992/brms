<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductsCollection;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request as Url;
use Request as Req;
use Symfony\Component\HttpFoundation\Response;
class ProductController extends Controller
{

 private $model; 

 public function  __construct() {

  $this->model = new Product;  

} 


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Req::has('Authorization') || Req::header('Authorization')) {
            $user = Auth::guard('api')->user();
        }
        try {
          $category = (Url::get('category') != 'null' && Url::get('category') != '') ? Url::get('category') : null;
           $value = Url::get('value');

          switch (true) {

            case ($category != null):
            $model =  $this->model::whereHas('category', function($q) use 
                ($category) {
                $q->where('name', $category);
            })->orderBy('created_at', 'desc')
            ->whereStatus(1)
            ->paginate(15);
            break;
           
            case ($value != 'null'):
            $model =  $this->model::where('title', 'LIKE', '%' . $value . '%')
            ->orderBy('created_at', 'desc')
            ->whereStatus(1)
            ->paginate(15);
            break;

            default:
            $model = $this->model->orderBy('created_at', 'desc')
            ->whereStatus(1)
            ->paginate(15);
        }
        $modelResource = ProductsCollection::collection($model);
        $response = [
            'pagination' => [
              'total' => $model->total(),
                'per_page' => $model->perPage(),
                'current_page' =>  $model->currentPage(),
                'last_page' =>  $model->lastPage(),
                "page_url" => "products?page=",
                'from' => $model->firstItem()
          ],
          'data' =>  $modelResource,
      ];
      return response()->json($response, Response::HTTP_OK);
  } catch(\Exception $e) {
   return response(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
}

}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Product::whereSlug($id)->first();
        $model =  new ProductResource($model);
        return response()->json($model, Response::HTTP_OK);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function newArrivals()
    {
        $response = ProductsCollection::
        collection($this->model->orderBy('created_at', 'desc')->take(10)
            ->whereStatus(1)
            ->get());
        return response()->json($response, Response::HTTP_OK);
    }

    public function categories()
    {
     $data = Category::pluck('name');
     return response()->json($data, Response::HTTP_OK);
 }

 //   public function singleProductDetails($slug) {
 // return response()->json($slug, Response::HTTP_OK);

 //   }



}
