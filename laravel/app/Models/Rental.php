<?php

namespace App\Models;

use App\Models\Product;
use App\Models\Transaction;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Rental extends Model
{
    //

	protected $table = 'rentals';

	protected $fillable = ['customer_id','status','transaction_id','product_id', 'item_received_date', 'requested_days','approved_days','date_start', 'date_end','penalty_days','total_rent_amount','penalty_amount','payment_status', 'cancelled_at', 'void_refunded_at', 'approved_at', 'completed_at', 'created_at', 'qty', 'approved_qty'];

	// public function getTotalRentAmountAttribute($value) {
	// 	return number_format($value, 2);
	// }


	public function getRentalStartEndAttribute() {


		if(!empty($this->date_start) && !empty($this->date_start)) {
			$start =  date('M j', strtotime($this->date_start));
			$end =  date('M j, Y', strtotime($this->date_end));
			return $start . ' - ' . $end;
		}

		return 'Not set';		

	}

	public function getDateTimeReceivedAttribute() {

		$result = '';

		if($this->item_received_date == null) {

			$result = 'not set';

		} else {

			$result =  date('M j, h:i A', strtotime($this->item_received_date));

		}

		return $result;

	}


	public function getStartDateMutatedAttribute() {

		$result = '';

		if($this->date_start !== null) {
			return $result =  date('Y-m-d', strtotime($this->date_start));
		} 

		return $result;
	}

	public function getEndDateMutatedAttribute() {

		$result = '';

		if($this->date_end !== null) {
			return $result =  date('Y-m-d', strtotime($this->date_end));
		} 

		return $result;

	}


	public function getCancelledAtMutatedAttribute() {

		$result = '';

		if($this->cancelled_at == null) {

			$result = 'not set';

		} else {

			$result =  date('M j, h:i A', strtotime($this->cancelled_at));

		}

		return $result;

	}

	public function getApprovedAtMutatedAttribute() {

		$result = '';

		if($this->approved_at == null) {

			$result = 'not set';

		} else {

			$result =  date('M j, h:i A', strtotime($this->approved_at));

		}

		return $result;

	}

	public function getItemReceivedDateMutatedAttribute() {

		$result = '';

		if($this->item_received_date == null) {

			$result = 'not set';

		} else {

			$result =  date('M j, h:i A', strtotime($this->item_received_date));

		}

		return $result;

	}

	public function getPenaltyAmountMutatedAttribute() {

		$result = '';

		if($this->penalty_amount == null || $this->penalty_amount == '') {

			$result = 'P 0.00';

		} else {

			$result = 'P ' . number_format($this->penalty_amount, 2); 

		}

		return $result;

	}

		public function getTotalRentAmountMutatedAttribute() {

		$result = '';

		if($this->total_rent_amount == null || $this->total_rent_amount == '') {

			$result = 'P 0.00';

		} else {

			$result = 'P ' . number_format($this->total_rent_amount, 2); 

		}

		return $result;

	}


		public function getCompletedAtMutatedAttribute() {

		$result = '';

		if($this->completed_at == null) {

			$result = 'not set';

		} else {

			$result =  date('M j, h:i A', strtotime($this->completed_at));

		}

		return $result;

	}

	public function getVoidRefundedMutatedAttribute() {

		$result = '';

		if($this->void_refunded_at == null) {

			$result = 'not set';

		} else {

			$result =  date('M j, h:i A', strtotime($this->void_refunded_at));

		}

		return $result;

	}

	public function getCreatedAtMutatedAttribute() {

		$result = '';

		if($this->created_at == null) {

			$result = 'not set';

		} else {

			$result =  date('M j, h:i A', strtotime($this->created_at));

		}

		return $result;

	}



	public function transaction()
	{
		return $this->belongsTo(Transaction::class, 'transaction_id');
	} 



	public function product()
	{
		return $this->belongsTo(Product::class, 'product_id');
	} 

	public function getDaysLeftAttribute() {

		$start = strtotime($this->date_start);

		$end =   strtotime($this->date_end);

		$difference = ceil(abs($end - $start) / 86400);
		
		return $difference;

	}

	public function getIsPaidAttribute() {

		return	$this->payment_status ? 'paid' : 'pending';

	}


	public function getDailyRateAttribute() {

		if($this->product()->value('initial_rate') == null  || $this->product()->value('initial_rate') == '') {

			return 'not set';

		}

		return $this->product()->value('initial_rate');
	}


	   public function customerCancelRequest()
    {
        return $this->belongsToMany(User::class, 'customer_cancel_request', 'rental_id' , 'user_id');
    }

    	public function customer()
	{
		return $this->belongsTo(User::class, 'customer_id');
	} 

    public function setTotalRentAmountAttribute($val) {

		$newVal = preg_replace("/[\$\,\s]/", '', $val);

		$this->attributes['total_rent_amount'] = $newVal;
	}

}
