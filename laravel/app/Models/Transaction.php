<?php

namespace App\Models;

use App\Models\Rental;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';

	protected $fillable = ['generated', 'user_id', 'created_at'];

	public function rentals() {

		return $this->hasMany(Rental::class, 'transaction_id');

	}
}
