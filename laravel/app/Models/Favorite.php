<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
	 protected $table = 'favorites';

    protected $fillable = [
         'product_id', 'user_id'
    ];
}
