<?php

namespace App\Models\Transactions;

use Illuminate\Database\Eloquent\Model;

class VoidRefunded extends Model
{
    protected $table = 'transactions_void_refunded';
}
