<?php

namespace App\Models\Transactions;

use Illuminate\Database\Eloquent\Model;

class OnGoing extends Model
{
    protected $table = 'transactions_on_going';
}
