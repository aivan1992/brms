<?php

namespace App\Models\Transactions;

use Illuminate\Database\Eloquent\Model;

class Completed extends Model
{
     protected $table = 'transactions_completed';
}
