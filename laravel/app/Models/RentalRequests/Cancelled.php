<?php

namespace App\Models\RentalRequests;

use Illuminate\Database\Eloquent\Model;

class Cancelled extends Model
{
    protected $table =  'rental_requests_cancelled';
}
