<?php

namespace App\Models\RentalRequests;

use Illuminate\Database\Eloquent\Model;

class Accepted extends Model
{
    protected $table =  'rental_requests_accepted';
}
