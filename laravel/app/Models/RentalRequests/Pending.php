<?php

namespace App\Models\RentalRequests;

use Illuminate\Database\Eloquent\Model;

class Pending extends Model
{
     protected $table = 'rental_requests_pendings';
}
