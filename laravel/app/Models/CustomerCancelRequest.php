<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerCancelRequest extends Model
{
    protected $table = 'customer_cancel_request';

    protected $fillable = [
         'rental_id', 'user_id'
    ];
}
