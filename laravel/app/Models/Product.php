<?php

namespace App\Models;

use App\Models\Category;
use App\Models\Image;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use App\User;

class Product extends Model
{
	
	use Sluggable;

	protected $table = 'products';

	protected $fillable = ['title', 'serial', 'description', 'slug','initial_rate', 'status','category_id', 'succeeding_days_rate'];

	protected $attributes = [
		'initial_rate' => 0
	];

	public function sluggable()
	{
		return [
			'slug' => [
				'source' => 'title'
			]
		];
	}     


	public function category()
	{
		return $this->belongsTo(Category::class, 'category_id');
	}  

	// 	public function images()
	// {
	// 	return $this->hasMany(Image::class, 'product_id');
	// }   

	// public function category()
	// {
	// 	return $this->belongsTo(category::class, 'category_id');
	// }     

	public function getDailyRateAttribute($value) {

		return number_format($value, 2);

	}


	public function setDailyRateAttribute($value) {

		$newVal = preg_replace("/[A-Za-z\,\s]/", '', $value);

		$this->attributes['initial_rate'] = $newVal;

	}

	public function getImagePathAttribute() {


		if(count($this->images)) {
			return  "/". $this->images()->value('directory_name') . '/'. $this->images()->value('file_name');
		}

	}

	public function images()
	{
		return $this->morphMany(Image::class, 'imageable');
	}


	// public function cart()
 //    {
 //        return $this->belongsToMany(User::class, 'carts', 'product_id', 'user_id');
 //    } 

	public function getInitialRateMutatedAttribute() {

		$result = '';

		if($this->initial_rate == null || $this->initial_rate == '') {

			$result = 'P 0.00';

		} else {

			$result = 'P ' . number_format($this->initial_rate, 2); 

		}

		return $result;

	}

	public function getSucceedingDaysRateMutatedAttribute() {

		$result = '';

		if($this->succeeding_days_rate == null || $this->succeeding_days_rate == '') {

			$result = 'P 0.00';

		} else {

			$result = 'P ' . number_format($this->succeeding_days_rate, 2); 

		}

		return $result;

	}

}
