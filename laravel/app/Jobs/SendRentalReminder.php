<?php

namespace App\Jobs;

use App\Mail\UserRentalReminder;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Symfony\Component\HttpFoundation\Response;

class SendRentalReminder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $endDate)
    {
     $this->email = $email;
     $this->endDate = $endDate;
 }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        \Mail::to($this->email)
        ->send(new UserRentalReminder($this->endDate));

        if(count(\Mail::failures()) > 0 ) {
            return response()->json(['error' => 'error occured!'], Response::HTTP_BAD_REQUEST);
        }

    }
}
